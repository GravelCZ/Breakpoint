/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.block.Block
 *  org.bukkit.block.BlockFace
 *  org.bukkit.block.BlockState
 *  org.bukkit.block.Sign
 *  org.bukkit.entity.EnderCrystal
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Projectile
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityShootBowEvent
 *  org.bukkit.event.entity.PlayerDeathEvent
 *  org.bukkit.event.entity.PotionSplashEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerRespawnEvent
 *  org.bukkit.event.player.PlayerTeleportEvent
 *  org.bukkit.event.player.PlayerTeleportEvent$TeleportCause
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.material.Button
 *  org.bukkit.material.MaterialData
 *  org.bukkit.projectiles.ProjectileSource
 */
package cz.GravelCZLP.Breakpoint.game.ctf;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.material.Button;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameListener;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.perks.Perk;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;

public class CTFListener extends GameListener {
	public CTFListener(Game game) {
		super(game, CTFGame.class);
	}

	public CTFListener(Game game, Class<? extends Game> gameClass) {
		super(game, gameClass);
	}

	@Override
	public void onPlayerDeath(PlayerDeathEvent event, BPPlayer bpPlayer) {
		Team team;
		CTFGame game = this.getGame();
		FlagManager flm = game.getFlagManager();
		if (flm.isHoldingFlag(bpPlayer)) {
			flm.dropFlag(bpPlayer);
		}
		if ((team = ((CTFProperties) bpPlayer.getGameProperties()).getTeam()) != null) {
			Player player = bpPlayer.getPlayer();
			Location loc = player.getLocation();
			Location effectLoc = loc.clone().add(0.0, 1.0, 0.0);
			team.displayDeathEffect(effectLoc);
		}
	}

	@Override
	public void onPlayerRespawn(PlayerRespawnEvent event, BPPlayer bpPlayer, boolean leaveAfterDeath) {
		CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
		CharacterType qct = bpPlayer.getQueueCharacter();
		CTFGame game = this.getGame();
		if (qct != null) {
			if (qct != null) {
				props.chooseCharacter(qct, false);
			}
			bpPlayer.setQueueCharacter(null);
		}
		if (leaveAfterDeath) {
			game.updateTeamMapViews();
		} else {
			game.spawn(bpPlayer);
		}
	}

	@Override
	public void onPlayerShootBow(EntityShootBowEvent event, BPPlayer bpPlayer) {
	}

	@Override
	public void onPlayerSplashedByPotion(PotionSplashEvent event, BPPlayer bpShooter, BPPlayer bpTarget) {
		CTFProperties targetProps = (CTFProperties) bpTarget.getGameProperties();
		Player target = bpTarget.getPlayer();
		CTFGame game = this.getGame();
		if (targetProps.isEnemy(bpShooter) && !game.hasRoundEnded()) {
			if (targetProps.hasSpawnProtection()) {
				Player shooter = bpShooter.getPlayer();
				shooter.sendMessage(MessageType.PVP_SPAWNKILLING.getTranslation().getValue(new Object[0]));
				event.setIntensity((LivingEntity) target, 0.0);
			}
		} else {
			event.setIntensity((LivingEntity) target, 0.0);
		}
	}

	@Override
	public void onEntityDamage(EntityDamageEvent dmgEvent) {
		Entity eVictim = dmgEvent.getEntity();
		if (eVictim instanceof EnderCrystal) {
			dmgEvent.setCancelled(true);
		}
		if (eVictim instanceof Player) {
			Player victim = (Player) eVictim;
			BPPlayer bpVictim = BPPlayer.get(victim);
			Game vGame = bpVictim.getGame();
			CTFGame game = this.getGame();
			if (!game.equals(vGame)) {
				return;
			}
			if (!(dmgEvent instanceof EntityDamageByEntityEvent)) {
				return;
			}
			EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) dmgEvent;
			Entity eDamager = event.getDamager();
			if (eDamager instanceof Projectile) {
				eDamager = (Entity) ((Projectile) eDamager).getShooter();
			}
			if (!(eDamager instanceof Player)) {
				event.setCancelled(true);
				return;
			}
			Player damager = (Player) eDamager;
			BPPlayer bpDamager = BPPlayer.get(damager);
			if (!bpVictim.isInGameWith(bpDamager)) {
				event.setCancelled(true);
				return;
			}
			CTFProperties victimProps = (CTFProperties) bpVictim.getGameProperties();
			if (!victimProps.isEnemy(bpDamager)) {
				eVictim.setFireTicks(0);
				event.setCancelled(true);
			}
		} else if (eVictim instanceof EnderCrystal) {
			dmgEvent.setCancelled(true);
			if (dmgEvent instanceof EntityDamageByEntityEvent) {
				CTFGame game = this.getGame();
				FlagManager flm = game.getFlagManager();
				EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) dmgEvent;
				flm.onTryFlagTake(event);
			}
		}
	}

	@Override
	public void onPlayerTeleport(PlayerTeleportEvent event, BPPlayer bpPlayer) {
		FlagManager flm;
		CTFGame game;
		PlayerTeleportEvent.TeleportCause cause = event.getCause();
		if (cause == PlayerTeleportEvent.TeleportCause.ENDER_PEARL
				&& (flm = (game = this.getGame()).getFlagManager()).isHoldingFlag(bpPlayer)) {
			Player player = bpPlayer.getPlayer();
			event.setCancelled(true);
			PlayerInventory inv = player.getInventory();
			inv.setItem(inv.getHeldItemSlot(), new ItemStack(Material.ENDER_PEARL));
			player.sendMessage(MessageType.OTHER_WARNPEARL.getTranslation().getValue(new Object[0]));
		}
	}

	@Override
	public void onPlayerRightClickItem(PlayerInteractEvent event, BPPlayer bpPlayer, ItemStack item) {
		CTFGame game;
		FlagManager flm;
		Material type = item.getType();
		if (type == Material.ENDER_PEARL && (flm = (game = this.getGame()).getFlagManager()).isHoldingFlag(bpPlayer)) {
			Player player = bpPlayer.getPlayer();
			event.setCancelled(true);
			player.sendMessage(MessageType.OTHER_WARNPEARL.getTranslation().getValue(new Object[0]));
		}
	}

	@Override
	public void onPlayerLeftClickItem(PlayerInteractEvent event, BPPlayer bpPlayer, ItemStack item) {
	}

	@Override
	public void onPlayerRightClickBlock(PlayerInteractEvent event, BPPlayer bpPlayer) {
		String[] lines;
		Sign sign;
		Block block = event.getClickedBlock();
		Material mat = block.getType();
		if (mat == Material.STONE_BUTTON) {
			Button button = (Button) block.getState().getData();
			Block attBlock = block.getRelative(button.getAttachedFace());
			if (attBlock.getType() == Material.WOOL) {
				this.clickedWoolButton(event, attBlock, bpPlayer);
			}
		} else if ((mat == Material.WALL_SIGN || mat == Material.SIGN_POST)
				&& ChatColor.stripColor((String) (lines = (sign = (Sign) block.getState()).getLines())[0])
						.equals(MessageType.CHARACTER_SELECT.getTranslation().getValue(new Object[0]))) {
			Player player = bpPlayer.getPlayer();
			CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
			Team team = props.getTeam();
			if (team != null) {
				CharacterType selectedCT = props.getCharacterType();
				if (selectedCT == null) {
					String rawCharType = ChatColor.stripColor((String) lines[1]);
					CharacterType charType = null;
					for (CharacterType ct : CharacterType.values()) {
						if (!rawCharType.equalsIgnoreCase(ct.getProperName()))
							continue;
						charType = ct;
						break;
					}
					if (charType != null) {
						String name = charType.getProperName();
						if (charType.requiresVIP() && !bpPlayer.getPlayer()
								.hasPermission("Breakpoint.kit." + charType.name().toLowerCase())) {
							player.sendMessage((Object) ChatColor.DARK_GRAY + "---");
							player.sendMessage(MessageType.LOBBY_CHARACTER_VIPSONLY.getTranslation().getValue(name));
							player.sendMessage((Object) ChatColor.DARK_GRAY + "---");
							return;
						}
						props.chooseCharacter(charType, true);
						player.sendMessage(MessageType.LOBBY_CHARACTER_SELECTED.getTranslation().getValue(name));
					} else {
						player.sendMessage(MessageType.LOBBY_CHARACTER_NOTFOUND.getTranslation().getValue(rawCharType));
					}
				} else {
					String charName = selectedCT.getProperName();
					player.sendMessage(MessageType.LOBBY_CHARACTER_ALREADYSELECTED.getTranslation().getValue(charName));
				}
			} else {
				player.sendMessage(MessageType.LOBBY_TEAM_WARN.getTranslation().getValue(new Object[0]));
			}
		}
	}

	@Override
	public void onPlayerPhysicallyInteractWithBlock(PlayerInteractEvent event, BPPlayer bpPlayer, Block blockBelow) {
		Material type = blockBelow.getType();
		byte data = blockBelow.getData();
		if (type == Material.WOOL && data == 2) {
			CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
			props.chooseRandomTeam();
		}
	}

	public void clickedWoolButton(PlayerInteractEvent event, Block block, BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		byte data = block.getData();
		if (data == 11) {
			if (!player.hasPermission("Breakpoint.ctf.teamSelect")) {
				event.setCancelled(true);
				player.sendMessage(MessageType.LOBBY_TEAM_SELECTVIPSONLY.getTranslation().getValue(new Object[0]));
				return;
			}
			CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
			Team team = props.getTeam();
			if (team == null) {
				CTFGame game = this.getGame();
				if (game.canJoinTeam(Team.BLUE)) {
					props.chooseTeam(Team.BLUE);
				} else {
					player.sendMessage(MessageType.LOBBY_TEAM_BALANCEJOINRED.getTranslation().getValue(new Object[0]));
				}
			}
		} else if (data == 14) {
			if (!player.hasPermission("Breakpoint.ctf.teamSelect")) {
				event.setCancelled(true);
				player.sendMessage(MessageType.LOBBY_TEAM_SELECTVIPSONLY.getTranslation().getValue(new Object[0]));
				return;
			}
			CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
			Team team = props.getTeam();
			if (team == null) {
				CTFGame game = this.getGame();
				if (game.canJoinTeam(Team.RED)) {
					props.chooseTeam(Team.RED);
				} else {
					player.sendMessage(MessageType.LOBBY_TEAM_BALANCEJOINBLUE.getTranslation().getValue(new Object[0]));
				}
			}
		}
	}

	@Override
	public boolean onPlayerChat(AsyncPlayerChatEvent event, BPPlayer bpPlayer) {
		String message;
		CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
		Team team = props.getTeam();
		if (team != null && (message = event.getMessage()).length() >= 1 && message.charAt(0) == '&'
				&& message.charAt(1) == '&') {
			CTFGame game = this.getGame();
			Player player = bpPlayer.getPlayer();
			event.setCancelled(true);
			String playerName = player.getName();
			Breakpoint.info("Team chat: " + playerName + ": " + message);
			game.sendTeamMessage(player, message);
			return true;
		}
		return false;
	}

	@Override
	public CTFGame getGame() {
		return (CTFGame) super.getGame();
	}

	@Override
	public void onPlayerMove(BPPlayer bpPlayer, Location from, Location to, PlayerMoveEvent e) {
		Perk.onMove(bpPlayer, e);
	}
}
