/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.GameMode
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.Server
 *  org.bukkit.Sound
 *  org.bukkit.entity.Arrow
 *  org.bukkit.entity.EnderCrystal
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.EntityType
 *  org.bukkit.entity.Fireball
 *  org.bukkit.entity.ItemFrame
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.Projectile
 *  org.bukkit.entity.SmallFireball
 *  org.bukkit.entity.SplashPotion
 *  org.bukkit.entity.ThrownPotion
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.entity.EntityCombustByEntityEvent
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityDamageEvent$DamageCause
 *  org.bukkit.event.entity.EntityShootBowEvent
 *  org.bukkit.event.entity.PlayerDeathEvent
 *  org.bukkit.event.entity.PotionSplashEvent
 *  org.bukkit.event.entity.ProjectileHitEvent
 *  org.bukkit.event.entity.ProjectileLaunchEvent
 *  org.bukkit.event.player.PlayerRespawnEvent
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 *  org.bukkit.projectiles.ProjectileSource
 *  org.bukkit.scheduler.BukkitScheduler
 *  org.bukkit.util.Vector
 */
package cz.GravelCZLP.Breakpoint.listeners;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.SplashPotion;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.achievements.Achievement;
import cz.GravelCZLP.Breakpoint.achievements.AchievementType;
import cz.GravelCZLP.Breakpoint.game.BPMap;
import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.FlagManager;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.managers.AbilityManager;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import cz.GravelCZLP.Breakpoint.managers.PlayerManager;
import cz.GravelCZLP.Breakpoint.managers.SoundManager;
import cz.GravelCZLP.Breakpoint.perks.Perk;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.Settings;
import cz.GravelCZLP.Breakpoint.sound.BPSound;
import cz.GravelCZLP.Breakpoint.statistics.PlayerStatistics;

public class PVPListener implements Listener {
	Breakpoint plugin;

	public PVPListener(Breakpoint p) {
		this.plugin = p;
	}

	@EventHandler
	public void onPotionSplash(PotionSplashEvent event) {
		ThrownPotion ePotion = event.getPotion();
		Entity eShooter = (Entity) ePotion.getShooter();
		if (eShooter instanceof Player) {
			Player shooter = (Player) eShooter;
			BPPlayer bpShooter = BPPlayer.get(shooter);
			if (bpShooter.isInGame()) {
				Game game = bpShooter.getGame();
				for (LivingEntity eTarget : event.getAffectedEntities()) {
					if (eTarget instanceof Player) {
						Player target = (Player) eTarget;
						BPPlayer bpTarget = BPPlayer.get(target);
						if (bpTarget.isInGameWith(bpShooter)) {
							game.getListener().onPlayerSplashedByPotion(event, bpShooter, bpTarget);
							continue;
						}
						event.setIntensity(eTarget, 0.0);
						continue;
					}
					event.setIntensity(eTarget, 0.0);
				}
			} else {
				event.setCancelled(true);
			}
		} else {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityShootBow(EntityShootBowEvent event) {
		LivingEntity eShooter = event.getEntity();
		if (!(eShooter instanceof Player)) {
			return;
		}
		Player shooter = (Player) eShooter;
		BPPlayer bpShooter = BPPlayer.get(shooter);
		Game game = bpShooter.getGame();
		if (game == null) {
			return;
		}
		game.getListener().onPlayerShootBow(event, bpShooter);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(PlayerDeathEvent event) {
		event.setDeathMessage(null);
		Player player = event.getEntity();
		BPPlayer bpPlayer = BPPlayer.get(player);
		Game pGame = bpPlayer.getGame();
		Player killer = player.getKiller();
		String playerPVPName = bpPlayer.getPVPName();
		if (bpPlayer.isPlaying()) {
			bpPlayer.getStatistics().increaseDeaths();
		}
		bpPlayer.setKilledThisLife(0);
		bpPlayer.setMultikills(0);
		bpPlayer.setLastTimeKilled(0L);
		if (killer != null) {
			BPPlayer bpKiller = BPPlayer.get(killer);
			if (bpPlayer.isInGameWith(bpKiller)) {
				Location kLoc = killer.getLocation();
				String killerName = killer.getName();
				String killerPVPName = bpKiller.getPVPName();
				PlayerStatistics kStats = bpKiller.getStatistics();
				GameProperties kProps = bpKiller.getGameProperties();
				if (kProps.hasCharacterType()) {
					CharacterType ct = kProps.getCharacterType();
					kStats.increaseKills(ct);
					Achievement.checkCharacterKills(bpKiller, ct);
				}
				kStats.increaseKills();
				bpKiller.addMoney(1, true, true);
				Achievement.checkKills(bpKiller);
				PlayerManager.executeMultikill(bpKiller, kLoc, 5);
				PlayerManager.executeKillingSpree(bpKiller, kLoc);
				killer.setLevel(killer.getLevel() + 1);
				if (pGame.getFirstBloodPlayerName() == null) {
					if (Bukkit.getOnlinePlayers().size() >= Bukkit.getOnlinePlayers().size() / 2) {
						bpKiller.checkAchievement(AchievementType.FIRST_BLOOD);
					}
					pGame.setFirstBloodPlayerName(killerName);
					pGame.broadcast((Object) ChatColor.DARK_RED + "" + (Object) ChatColor.BOLD + "> FIRST BLOOD! "
							+ (String) killerPVPName + (Object) ChatColor.DARK_RED + (Object) ChatColor.BOLD + " <",
							false);
					PlayerManager.spawnRandomlyColoredFirework(kLoc);
					SoundManager.playSoundAt(BPSound.FIRST_BLOOD, kLoc);
				}
				pGame.setLastBloodPlayerName(killerName);
				killer.playSound(kLoc, Sound.BLOCK_NOTE_PLING, 1.0f, 4.0f);
				if (!bpPlayer.getSettings().hasDeathMessages()) {
					player.sendMessage(MessageType.PVP_KILLINFO_KILLEDBY.getTranslation().getValue(killerPVPName));
				}
				if (!bpKiller.getSettings().hasDeathMessages()) {
					killer.sendMessage(MessageType.PVP_KILLINFO_YOUKILLED.getTranslation().getValue(playerPVPName));
				}
				pGame.broadcastDeathMessage(playerPVPName, killerPVPName);
				if (pGame.getPlayers().size() >= 5 && bpPlayer.equals(bpKiller.getLastTimeKilledBy())) {
					bpKiller.setLastTimeKilledBy(null);
					killer.sendMessage(MessageType.PVP_PAYBACK.getTranslation().getValue(new Object[0]));
					killer.playSound(kLoc, Sound.BLOCK_ANVIL_LAND, 0.5f, 0.5f);
				}
				bpPlayer.setLastTimeKilledBy(bpKiller);
				bpPlayer.getLastTimeDamagedBy().remove(bpKiller);
			}
		} else {
			Settings pSettings = bpPlayer.getSettings();
			if (!pSettings.hasDeathMessages()) {
				player.sendMessage(MessageType.PVP_KILLINFO_DEATH.getTranslation().getValue(new Object[0]));
			}
			if (pGame != null) {
				pGame.broadcastDeathMessage(playerPVPName);
			}
		}
		HashMap<BPPlayer, Long> lastTimeDamagedBy = bpPlayer.getLastTimeDamagedBy();
		long now = System.currentTimeMillis();
		for (Entry<BPPlayer, Long> entry : lastTimeDamagedBy.entrySet()) {
			Player other;
			BPPlayer bpOther;
			long when = (Long) entry.getValue();
			if (now - when > 5000L || (other = (bpOther = (BPPlayer) entry.getKey()).getPlayer()) == null)
				continue;
			Location oLoc = other.getEyeLocation();
			other.playSound(oLoc, Sound.BLOCK_NOTE_PLING, 0.5f, 1.0f);
			bpOther.getStatistics().increaseAssists();
			other.sendMessage(MessageType.PVP_KILLINFO_ASSIST.getTranslation().getValue(playerPVPName));
		}
		lastTimeDamagedBy.clear();
		if (bpPlayer.isPlaying()) {
			bpPlayer.decreasePerkLives(true);
		}
		bpPlayer.clearAfkSecondsToKick();
		event.getDrops().clear();
		event.setDroppedExp(0);
		if (pGame != null) {
			pGame.getListener().onPlayerDeath(event, bpPlayer);
		}
		if (bpPlayer.isLeaveAfterDeath()) {
			if (pGame != null) {
				pGame.onPlayerLeaveGame(bpPlayer);
			}
			bpPlayer.updateArmorMinutesLeft();
			bpPlayer.setLeaveAfterDeath(false);
			bpPlayer.reset();
			bpPlayer.setLeaveAfterDeath(false);
		}
		PlayerManager.respawnWithDelay(player);
	}

	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		BPPlayer bpPlayer = BPPlayer.get(player);
		boolean leaveAfterDeath = bpPlayer.isLeaveAfterDeath();
		Game game = bpPlayer.getGame();
		Location stl = bpPlayer.getSingleTeleportLocation();
		if (game != null) {
			game.getListener().onPlayerRespawn(event, bpPlayer, leaveAfterDeath);
		} else {
			bpPlayer.spawn();
		}
		if (stl != null) {
			bpPlayer.teleport(stl);
			bpPlayer.setSingleTeleportLocation(null);
		}
	}

	private void hitByArrow(EntityDamageByEntityEvent event, BPPlayer bpDamager, Player damager, BPPlayer bpVictim,
			Player victim) {
		Arrow arrow = (Arrow) event.getDamager();
		GameProperties damagerProps = bpDamager.getGameProperties();
		CharacterType ct = damagerProps.getCharacterType();
		if ((ct == CharacterType.ARCHER || ct == CharacterType.PYRO)
				&& AbilityManager.isHeadshot(damager.getLocation(), victim.getLocation(), arrow)) {
			String displayName = bpVictim.getPVPName();
			event.setDamage(event.getDamage() * 2.0);
			AbilityManager.playHeadshotEffect(victim);
			damager.sendMessage(MessageType.PVP_HEADSHOT.getTranslation().getValue(displayName));
			SoundManager.playSoundAt(BPSound.HEADSHOT, damager.getLocation());
		}
	}

	@SuppressWarnings("deprecation")
	private void hitByFireball(EntityDamageByEntityEvent event, Player damager, Player victim) {
		double dmg = event.getDamage() * 1.2;
		event.setDamage(dmg);
		EntityDamageByEntityEvent dmgCause = new EntityDamageByEntityEvent((Entity) damager, (Entity) victim,
				EntityDamageEvent.DamageCause.ENTITY_ATTACK, dmg);
		victim.setLastDamageCause((EntityDamageEvent) dmgCause);
	}

	@SuppressWarnings("deprecation")
	private void hitBySmallFireball(EntityDamageByEntityEvent event, Player damager, Player victim) {
		double dmg = event.getDamage();
		EntityDamageByEntityEvent dmgCause = new EntityDamageByEntityEvent((Entity) damager, (Entity) victim,
				EntityDamageEvent.DamageCause.ENTITY_ATTACK, dmg);
		victim.setLastDamageCause((EntityDamageEvent) dmgCause);
	}

	@SuppressWarnings("deprecation")
	private void hitByEntityExplosion(EntityDamageByEntityEvent event, BPPlayer bpDamager, Player shooter,
			BPPlayer bpVictim, Player victim) {
		Entity exploded = event.getDamager();
		if (exploded instanceof Projectile && shooter != null) {
			CTFProperties damagerProps = (CTFProperties) bpDamager.getGameProperties();
			if (damagerProps.isEnemy(bpVictim)) {
				EntityDamageByEntityEvent dmgCause = new EntityDamageByEntityEvent((Entity) shooter, (Entity) victim,
						EntityDamageEvent.DamageCause.ENTITY_ATTACK, event.getDamage());
				victim.setLastDamageCause((EntityDamageEvent) dmgCause);
				return;
			}
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		if (entity instanceof ItemFrame) {
			this.onEmptyItemFrame(event);
		}
	}

	public void onEmptyItemFrame(EntityDamageByEntityEvent event) {
		Player damager;
		Entity eDamager = event.getDamager();
		if (eDamager instanceof Player && (damager = (Player) eDamager).hasPermission("Breakpoint.build")
				&& damager.getGameMode() == GameMode.CREATIVE) {
			return;
		}
		event.setCancelled(true);
	}

	@EventHandler(ignoreCancelled = true)
	public void onEntityDamage(EntityDamageEvent dmgEvent) {
		Entity eVictim = dmgEvent.getEntity();
		BPPlayer bpVictim = null;

		if (eVictim instanceof Player) {
			Player victim = (Player) eVictim;
			bpVictim = BPPlayer.get(victim);

			if (dmgEvent instanceof EntityDamageByEntityEvent) {
				EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) dmgEvent;
				Entity eDamager = event.getDamager();

				if (eVictim.equals(eDamager)) {
					event.setCancelled(true);
					return;
				}
			}

			if (!bpVictim.isPlaying()) {
				DamageCause cause = dmgEvent.getCause();

				if (cause != DamageCause.SUICIDE && cause != DamageCause.VOID) {
					dmgEvent.setCancelled(true);
				}

				return;
			} else {
				Game game = bpVictim.getGame();

				if (game.hasRoundEnded()) {
					dmgEvent.setCancelled(true);
				}
			}
		}

		for (Game game : GameManager.getGames()) {
			game.getListener().onEntityDamage(dmgEvent);
		}

		if (dmgEvent.isCancelled()) {
			return;
		}

		if (eVictim instanceof Player) {
			Player victim = (Player) eVictim;
			Game game = bpVictim.getGame();

			if (dmgEvent instanceof EntityDamageByEntityEvent) {
				EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) dmgEvent;
				Entity eDamager = event.getDamager();
				Projectile projectileOfDamager = null;

				if (eDamager instanceof Projectile) {
					projectileOfDamager = (Projectile) eDamager;
					eDamager = (Entity) projectileOfDamager.getShooter();

					if (eDamager == null) {
						event.setCancelled(true);
						return;
					}
				}

				if (!(eDamager instanceof Player)) {
					event.setCancelled(true);
					return;
				}

				Player damager = (Player) eDamager;
				BPPlayer bpDamager = BPPlayer.get(damager);

				if (!bpVictim.isInGameWith(bpDamager)) {
					event.setCancelled(true);
					return;
				}

				if (bpVictim.getGameProperties().hasSpawnProtection()) {
					damager.sendMessage(MessageType.PVP_SPAWNKILLING.getTranslation().getValue());
					event.setCancelled(true);
					return;
				}

				PlayerInventory inv = victim.getInventory();
				ItemStack helmet = inv.getHelmet();

				if (helmet != null) {
					Material mat = helmet.getType();
					if (mat == Material.SKULL) {
						event.setDamage(event.getDamage() * (18.0 / 19.0));
					}
				}

				if (projectileOfDamager != null) {
					Perk.onDamageDealtByProjectile(bpDamager, event);
					Perk.onDamageTakenFromProjectile(bpVictim, event);

					if (projectileOfDamager instanceof Arrow) {
						hitByArrow(event, bpDamager, damager, bpVictim, victim);
					} else if (projectileOfDamager instanceof Fireball) {
						DamageCause dmgCause = event.getCause();

						if (dmgCause == DamageCause.ENTITY_EXPLOSION) {
							hitByEntityExplosion(event, bpDamager, damager, bpVictim, victim);
						} else {
							hitByFireball(event, damager, victim);
						}
					} else if (eDamager instanceof SmallFireball) {
						hitBySmallFireball(event, damager, victim);
					}
				} else {
					Perk.onDamageDealtByPlayer(bpDamager, event);
					Perk.onDamageTakenFromPlayer(bpVictim, event);
				}

				Perk.onDamageDealtByEntity(bpDamager, event);
				Perk.onDamageTakenFromEntity(bpVictim, event);

				if (!event.isCancelled()) {
					bpVictim.getLastTimeDamagedBy().put(bpDamager, System.currentTimeMillis());
				}
			} else {
				DamageCause dmgCause = dmgEvent.getCause();

				if (dmgCause == DamageCause.FALL) {
					BPMap map = game.getCurrentMap();
					double fallDamageMultiplier = map.getFallDamageMultiplier();
					dmgEvent.setDamage(dmgEvent.getDamage() * fallDamageMultiplier);
				}
			}
		}
	}

	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		Projectile proj;
		EntityType et = event.getEntityType();
		Entity hited = event.getHitEntity();
		if (hited != null && hited.getType() == EntityType.ENDER_CRYSTAL) {
			for (Game game : GameManager.getGames()) {
				if (game.getType() != GameType.CTF)
					continue;
				CTFGame ctfGame = (CTFGame) game;
				FlagManager flm = ctfGame.getFlagManager();
				Team t = flm.getFlagTeam((EnderCrystal) hited);
				boolean defLoc = true;
				Location enderCrystalLoc = null;
				if (flm.isAtDefaultLocation((EnderCrystal) hited)) {
					defLoc = true;
				} else {
					defLoc = false;
					enderCrystalLoc = ((EnderCrystal) hited).getLocation();
				}
				flm.removeFlag(t);
				if (defLoc) {
					flm.spawnFlagAtDefaultLocation(t);
					continue;
				}
				flm.spawnFlag(enderCrystalLoc, t);
			}
		}
		if (et == EntityType.FIREBALL) {
			if (hited != null) {
				proj = event.getEntity();
				AbilityManager.fireballHit((Fireball) proj);
				Vector vec = hited.getVelocity();
				vec.setY(vec.getY() * 2.5);
				vec.setX(0);
				vec.normalize();
				hited.setVelocity(vec);
			}
		} else if (et == EntityType.SMALL_FIREBALL) {
			proj = event.getEntity();
			AbilityManager.smallFireballHit((SmallFireball) proj);
		} else if (et == EntityType.ARROW) {
			final Arrow arrow = (Arrow) event.getEntity();
			Bukkit.getScheduler().scheduleSyncDelayedTask((Plugin) Breakpoint.getInstance(), new Runnable() {

				@Override
				public void run() {
					arrow.remove();
				}
			}, 10L);
		}
	}

	@EventHandler
	public void playerRespawnDelayer(final PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		event.setRespawnLocation(player.getLocation());
		this.plugin.getServer().getScheduler().scheduleSyncDelayedTask((Plugin) this.plugin, new Runnable() {

			@Override
			public void run() {
				PVPListener.this.onPlayerRespawn(event);
			}
		}, 1L);
	}

	@EventHandler
	public void onPotionThrow(ProjectileLaunchEvent e) {
		if (e.getEntityType() == EntityType.SPLASH_POTION) {
			SplashPotion sp = (SplashPotion) e.getEntity();
			sp.setVelocity(sp.getVelocity().multiply(1.6));
			sp.setBounce(false);
		}
		if (e.getEntityType() == EntityType.FIREBALL) {
			Fireball fb = (Fireball) e.getEntity();
			fb.setBounce(false);
		}
		if (e.getEntityType() == EntityType.ARROW) {
			Arrow arrow = (Arrow) e.getEntity();
			arrow.setBounce(false);
		}
	}

	@EventHandler
	public void onPlayerSetOnFire(EntityCombustByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			BPPlayer bpPlayer = BPPlayer.get(p);
			if (bpPlayer != null) {
				if (bpPlayer.isInGame()) {
					if (bpPlayer.getGameProperties().getCharacterType() == CharacterType.PYRO) {
						e.setDuration(e.getDuration() / 3);
					}
				}
			}
		}
	}

}
