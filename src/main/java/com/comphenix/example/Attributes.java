/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.comphenix.protocol.utility.MinecraftReflection
 *  com.comphenix.protocol.wrappers.nbt.NbtBase
 *  com.comphenix.protocol.wrappers.nbt.NbtCompound
 *  com.comphenix.protocol.wrappers.nbt.NbtFactory
 *  com.comphenix.protocol.wrappers.nbt.NbtList
 *  com.comphenix.protocol.wrappers.nbt.NbtType
 *  com.google.common.base.Function
 *  com.google.common.base.Objects
 *  com.google.common.base.Preconditions
 *  com.google.common.collect.Iterators
 *  com.google.common.collect.Maps
 *  javax.annotation.Nonnull
 *  javax.annotation.Nullable
 *  org.bukkit.inventory.ItemStack
 */
package com.comphenix.example;

import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.nbt.NbtBase;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;
import com.comphenix.protocol.wrappers.nbt.NbtList;
import com.comphenix.protocol.wrappers.nbt.NbtType;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterators;
import com.google.common.collect.Maps;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.bukkit.inventory.ItemStack;

public class Attributes {
	private final ItemStack stack;
	private final NbtList<Map<String, NbtBase<?>>> attributes;

	public Attributes(ItemStack stack) {
		this.stack = MinecraftReflection.getBukkitItemStack((Object) stack);
		NbtCompound nbt = (NbtCompound) NbtFactory.fromItemTag((ItemStack) this.stack);
		this.attributes = nbt.getListOrDefault("AttributeModifiers");
		this.attributes.setElementType(NbtType.TAG_COMPOUND);
	}

	public ItemStack getStack() {
		return this.stack;
	}

	public int size() {
		return this.attributes.size();
	}

	public void add(Attribute attribute) {
		this.attributes.add((NbtBase) attribute.data);
	}

	public boolean remove(Attribute attribute) {
		UUID uuid = attribute.getUUID();
		Iterator<Attribute> it = this.values().iterator();
		while (it.hasNext()) {
			if (!Objects.equal((Object) it.next().getUUID(), (Object) uuid))
				continue;
			it.remove();
			return true;
		}
		return false;
	}

	public void clear() {
		((List) this.attributes.getValue()).clear();
	}

	public Attribute get(int index) {
		return new Attribute((NbtCompound) ((List) this.attributes.getValue()).get(index));
	}

	public Iterable<Attribute> values() {
		return new Iterable<Attribute>() {

			@Override
			public Iterator<Attribute> iterator() {
				return Iterators.transform(((List) Attributes.this.attributes.getValue()).iterator(),
						(Function) new Function<NbtBase<Map<String, NbtBase<?>>>, Attribute>() {

							public Attribute apply(@Nullable NbtBase<Map<String, NbtBase<?>>> element) {
								return new Attribute((NbtCompound) element);
							}
						});
			}

		};
	}

	public static class Attribute {
		private final NbtCompound data;

		private Attribute(Builder builder) {
			this.data = NbtFactory.ofCompound((String) "");
			this.setAmount(builder.amount);
			this.setOperation(builder.operation);
			this.setAttributeType(builder.type);
			this.setName(builder.name);
			this.setUUID(builder.uuid);
		}

		private Attribute(NbtCompound data) {
			this.data = data;
		}

		public double getAmount() {
			return this.data.getDouble("Amount");
		}

		public void setAmount(double amount) {
			this.data.put("Amount", amount);
		}

		public Operation getOperation() {
			return Operation.fromId(this.data.getInteger("Operation"));
		}

		public void setOperation(@Nonnull Operation operation) {
			Preconditions.checkNotNull((Object) ((Object) operation), (Object) "operation cannot be NULL.");
			this.data.put("Operation", operation.getId());
		}

		public AttributeType getAttributeType() {
			return AttributeType.fromId(this.data.getString("AttributeName"));
		}

		public void setAttributeType(@Nonnull AttributeType type) {
			Preconditions.checkNotNull((Object) type, (Object) "type cannot be NULL.");
			this.data.put("AttributeName", type.getMinecraftId());
		}

		public String getName() {
			return this.data.getString("Name");
		}

		public void setName(@Nonnull String name) {
			this.data.put("Name", name);
		}

		public UUID getUUID() {
			return new UUID(this.data.getLong("UUIDMost"), this.data.getLong("UUIDLeast"));
		}

		public void setUUID(@Nonnull UUID id) {
			Preconditions.checkNotNull((Object) id, (Object) "id cannot be NULL.");
			this.data.put("UUIDLeast", id.getLeastSignificantBits());
			this.data.put("UUIDMost", id.getMostSignificantBits());
		}

		public static Builder newBuilder() {
			return new Builder().uuid(UUID.randomUUID()).operation(Operation.ADD_NUMBER);
		}

		public static class Builder {
			private double amount;
			private Operation operation = Operation.ADD_NUMBER;
			private AttributeType type;
			private String name;
			private UUID uuid;

			private Builder() {
			}

			public Builder amount(double amount) {
				this.amount = amount;
				return this;
			}

			public Builder operation(Operation operation) {
				this.operation = operation;
				return this;
			}

			public Builder type(AttributeType type) {
				this.type = type;
				return this;
			}

			public Builder name(String name) {
				this.name = name;
				return this;
			}

			public Builder uuid(UUID uuid) {
				this.uuid = uuid;
				return this;
			}

			public Attribute build() {
				return new Attribute(this);
			}
		}

	}

	public static class AttributeType {
		private static ConcurrentMap<String, AttributeType> LOOKUP = Maps.newConcurrentMap();
		public static final AttributeType GENERIC_MAX_HEALTH = new AttributeType("generic.maxHealth").register();
		public static final AttributeType GENERIC_FOLLOW_RANGE = new AttributeType("generic.followRange").register();
		public static final AttributeType GENERIC_ATTACK_DAMAGE = new AttributeType("generic.attackDamage").register();
		public static final AttributeType GENERIC_MOVEMENT_SPEED = new AttributeType("generic.movementSpeed")
				.register();
		public static final AttributeType GENERIC_KNOCKBACK_RESISTANCE = new AttributeType(
				"generic.knockbackResistance").register();
		private final String minecraftId;

		public AttributeType(String minecraftId) {
			this.minecraftId = minecraftId;
		}

		public String getMinecraftId() {
			return this.minecraftId;
		}

		public AttributeType register() {
			AttributeType old = LOOKUP.putIfAbsent(this.minecraftId, this);
			return old != null ? old : this;
		}

		public static AttributeType fromId(String minecraftId) {
			return LOOKUP.get(minecraftId);
		}

		public static Iterable<AttributeType> values() {
			return LOOKUP.values();
		}
	}

	public static enum Operation {
		ADD_NUMBER(0), MULTIPLY_PERCENTAGE(1), ADD_PERCENTAGE(2);

		private int id;

		private Operation(int id) {
			this.id = id;
		}

		public int getId() {
			return this.id;
		}

		public static Operation fromId(int id) {
			for (Operation op : Operation.values()) {
				if (op.getId() != id)
					continue;
				return op;
			}
			throw new IllegalArgumentException("Corrupt operation ID " + id + " detected.");
		}
	}

}
