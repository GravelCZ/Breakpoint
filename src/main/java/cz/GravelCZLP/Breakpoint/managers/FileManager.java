/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.BookMeta
 *  org.bukkit.inventory.meta.ItemMeta
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class FileManager {
	public static BookMeta loadWikiBook(BookMeta im) {
		String path = "plugins/Breakpoint/wikiBook/";
		ArrayList<String> contents = new ArrayList<String>();
		int page = 0;
		File file = new File(path + page + ".txt");
		while (file.exists()) {
			String pageContent = FileManager.getString(file);
			contents.add(pageContent);
			file = new File(path + ++page + ".txt");
		}
		im.setPages(contents);
		return im;
	}

	public static void saveWikiBook() {
		String path = "plugins/Breakpoint/wikiBook/";
		BookMeta im = (BookMeta) InventoryMenuManager.wikiBook.getItemMeta();
		List contents = im.getPages();
		for (int page = 0; page < contents.size(); ++page) {
			File file = new File(path + page + ".txt");
			File pathFile = new File(path);
			pathFile.mkdirs();
			try {
				if (!file.exists()) {
					file.createNewFile();
				}
				FileManager.setString(file, (String) contents.get(page));
				continue;
			} catch (IOException e) {
				Breakpoint.warn("Wiki book did not save properly.");
				e.printStackTrace();
			}
		}
	}

	public static void setString(File file, String s) throws IOException {
		FileWriter ryt = new FileWriter(file);
		BufferedWriter out = new BufferedWriter(ryt);
		out.write(s);
		out.close();
	}

	public static String getString(File file) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (Exception e) {
			e.printStackTrace();
			return "Error when reading file '" + file.getPath() + "'.";
		}
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				sb.append('\n');
				line = br.readLine();
			}
			br.close();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "Error when reading file '" + file.getPath() + "'.";
		}
	}

	public static void trySaveYaml(YamlConfiguration yaml, File file) {
		try {
			yaml.save(file);
		} catch (Exception e) {
			Breakpoint.warn("Error when saving '" + file.getAbsolutePath() + "'.");
			e.printStackTrace();
		}
	}
}
