/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.Sound
 *  org.bukkit.World
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.game.cw;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFMap;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.FlagManager;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.game.cw.CWProperties;
import cz.GravelCZLP.Breakpoint.game.cw.CWScheduler;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.SoundManager;
import cz.GravelCZLP.Breakpoint.perks.Perk;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.clans.Clan;
import cz.GravelCZLP.Breakpoint.players.clans.ClanChallenge;
import cz.GravelCZLP.Breakpoint.sound.BPSound;
import cz.GravelCZLP.Breakpoint.statistics.CWMatchResult;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class CWGame extends CTFGame {
	private final int[] wins = new int[2];
	private int round;
	private boolean begun;
	private final CWScheduler scheduler;
	private ClanChallenge day;

	public CWGame(String name, Location signLoc, Location characterSelectionLocation, LinkedList<CTFMap> maps,
			LinkedList<ClanChallenge> days) {
		super(GameType.CW, name, signLoc, null, characterSelectionLocation, maps, false);
		this.scheduler = new CWScheduler(this, days);
		this.setDay();
	}

	public CWGame(String name, Location signLoc) {
		this(name, signLoc, null, new LinkedList<CTFMap>(), new LinkedList<ClanChallenge>());
	}

	@Override
	public boolean isPlayable() {
		if (!super.isSuperPlayable()) {
			return false;
		}
		return this.getFlagManager() != null && this.characterSelectionLocation != null;
	}

	@Override
	public void changeMapExtra() {
		super.changeMapExtra();
		if (this.canBegin() && !this.hasBegun()) {
			this.broadcastGlobally(MessageType.CW_BROADCAST_MATCHSTART.getTranslation().getValue(new Object[0]));
			this.begun = true;
		}
	}

	@Override
	protected void endRoundExtra() {
		FlagManager flm = this.getFlagManager();
		flm.removeFlags();
		flm.removeHolders();
		int[] score = flm.getScore();
		this.broadcast((Object) ChatColor.GRAY + "---------------------------------");
		if (!this.hasBegun()) {
			if (score[0] == score[1]) {
				this.broadcast(MessageType.RESULT_CTF_DRAW.getTranslation().getValue(new Object[0]));
				SoundManager.playTeamSound(this, Sound.ENTITY_ARROW_HIT, 16.0f, 0.5f, Team.RED);
				SoundManager.playTeamSound(this, Sound.ENTITY_ARROW_HIT, 16.0f, 0.5f, Team.BLUE);
			} else if (score[0] > score[1]) {
				this.broadcast(MessageType.RESULT_CTF_WIN_RED.getTranslation().getValue(new Object[0]));
				SoundManager.playTeamSound(this, Sound.ENTITY_ENDERDRAGON_DEATH, 16.0f, 0.5f, Team.BLUE);
				SoundManager.playTeamSound(this, Sound.ENTITY_PLAYER_LEVELUP, 16.0f, 4.0f, Team.RED);
				this.spawnFireworks(Team.RED);
				if (score[1] <= 0) {
					SoundManager.playTeamSound(BPSound.FLAWLESS_VICTORY, this, Team.RED);
					SoundManager.playTeamSound(BPSound.HUMILIATING_DEFEAT, this, Team.BLUE);
				}
			} else if (score[1] > score[0]) {
				this.broadcast(MessageType.RESULT_CTF_WIN_BLUE.getTranslation().getValue(new Object[0]));
				SoundManager.playTeamSound(this, Sound.ENTITY_ENDERDRAGON_DEATH, 16.0f, 0.5f, Team.RED);
				SoundManager.playTeamSound(this, Sound.ENTITY_PLAYER_LEVELUP, 16.0f, 4.0f, Team.BLUE);
				this.spawnFireworks(Team.BLUE);
				if (score[0] <= 0) {
					SoundManager.playTeamSound(BPSound.FLAWLESS_VICTORY, this, Team.BLUE);
					SoundManager.playTeamSound(BPSound.HUMILIATING_DEFEAT, this, Team.RED);
				}
			}
		} else {
			if (score[0] == score[1]) {
				Configuration config = Breakpoint.getBreakpointConfig();
				this.broadcastGlobally(MessageType.RESULT_CW_DRAW.getTranslation().getValue(new Object[0]));
				SoundManager.playTeamSound(this, Sound.ENTITY_ARROW_HIT, 16.0f, 0.5f, Team.RED);
				SoundManager.playTeamSound(this, Sound.ENTITY_ARROW_HIT, 16.0f, 0.5f, Team.BLUE);
				if (Calendar.getInstance().get(11) >= config.getCWEndHour()) {
					this.endMatch();
					return;
				}
			} else if (score[0] > score[1]) {
				Clan[] clans = this.day.getClans();
				Configuration config = Breakpoint.getBreakpointConfig();
				this.broadcastGlobally(MessageType.RESULT_CW_WIN.getTranslation().getValue(clans[0].getColoredName()));
				SoundManager.playTeamSound(this, Sound.ENTITY_ENDERDRAGON_HURT, 16.0f, 0.5f, Team.BLUE);
				SoundManager.playTeamSound(this, Sound.ENTITY_PLAYER_LEVELUP, 16.0f, 4.0f, Team.RED);
				this.spawnFireworks(Team.RED);
				if (score[1] <= 0) {
					SoundManager.playTeamSound(BPSound.FLAWLESS_VICTORY, this, Team.RED);
					SoundManager.playTeamSound(BPSound.HUMILIATING_DEFEAT, this, Team.BLUE);
				}
				int[] arrn = this.wins;
				arrn[0] = arrn[0] + 1;
				if (arrn[0] >= config.getCWWinLimit() || Calendar.getInstance().get(11) >= config.getCWEndHour()) {
					this.endMatch();
					return;
				}
			} else if (score[1] > score[0]) {
				Clan[] clans = this.day.getClans();
				Configuration config = Breakpoint.getBreakpointConfig();
				this.broadcastGlobally(
						MessageType.RESULT_CTF_WIN_BLUE.getTranslation().getValue(clans[1].getColoredName()));
				SoundManager.playTeamSound(this, Sound.ENTITY_ENDERDRAGON_HURT, 16.0f, 0.5f, Team.RED);
				SoundManager.playTeamSound(this, Sound.ENTITY_PLAYER_LEVELUP, 16.0f, 4.0f, Team.BLUE);
				this.spawnFireworks(Team.BLUE);
				if (score[0] <= 0) {
					SoundManager.playTeamSound(BPSound.FLAWLESS_VICTORY, this, Team.BLUE);
					SoundManager.playTeamSound(BPSound.HUMILIATING_DEFEAT, this, Team.RED);
				}
				int[] arrn = this.wins;
				arrn[1] = arrn[1] + 1;
				if (arrn[1] >= config.getCWWinLimit() || Calendar.getInstance().get(11) >= config.getCWEndHour()) {
					this.endMatch();
					return;
				}
			}
			this.broadcastGlobally(
					MessageType.RESULT_CW_MATCH_SCORES.getTranslation().getValue(this.wins[0], this.wins[1]));
		}
		this.broadcast((Object) ChatColor.GRAY + "---------------------------------");
	}

	public void endMatch() {
		Clan[] clans = this.day.getClans();
		if (this.wins[0] > this.wins[1]) {
			Configuration config = Breakpoint.getBreakpointConfig();
			this.broadcastGlobally(
					MessageType.RESULT_CW_MATCH_WIN.getTranslation().getValue(clans[0].getColoredName()));
			this.awardPlayersInTeam(Team.RED, config.getCWEmeraldsForTotalWin());
		} else if (this.wins[1] > this.wins[0]) {
			Configuration config = Breakpoint.getBreakpointConfig();
			this.broadcastGlobally(
					MessageType.RESULT_CW_MATCH_WIN.getTranslation().getValue(clans[1].getColoredName()));
			this.awardPlayersInTeam(Team.BLUE, config.getCWEmeraldsForTotalWin());
		} else {
			this.broadcastGlobally(MessageType.RESULT_CW_MATCH_DRAW.getTranslation().getValue(new Object[0]));
		}
		this.broadcastGlobally(
				MessageType.RESULT_CW_MATCH_SCORES.getTranslation().getValue(this.wins[0], this.wins[1]));
		clans[0].addResult(new CWMatchResult(clans[1], new int[] { this.wins[0], this.wins[1] }));
		clans[1].addResult(new CWMatchResult(clans[0], new int[] { this.wins[1], this.wins[0] }));
		this.begun = false;
		this.setRound(0);
		this.wins[0] = 0;
		this.wins[1] = 0;
	}

	@Override
	public void join(BPPlayer bpPlayer) throws Exception {
		if (this.day == null) {
			throw new Exception(MessageType.LOBBY_GAME_CW_NOTALLOWED.getTranslation().getValue(new Object[0]));
		}
		Team team = this.day.getTeam(bpPlayer);
		if (team == null) {
			throw new Exception(MessageType.LOBBY_GAME_CW_NOTALLOWED.getTranslation().getValue(new Object[0]));
		}
		if (!this.canJoinTeam(team)) {
			throw new Exception(
					MessageType.LOBBY_GAME_CW_TEAMFULL.getTranslation().getValue(this.day.getMaximumPlayers()));
		}
		CWProperties props = new CWProperties(this, bpPlayer);
		bpPlayer.setGameProperties(props);
		super.superJoin(bpPlayer);
		props.chooseTeam(team);
	}

	@Override
	public boolean canJoinTeam(Team team) {
		int maxPlayers = this.day.getMaximumPlayers();
		int players = this.getPlayersInTeam(team).size();
		return players < maxPlayers;
	}

	@Override
	public void spawn(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		if (player.isDead()) {
			return;
		}
		CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
		Team team = props.getTeam();
		bpPlayer.setSpawnTime(System.currentTimeMillis());
		bpPlayer.purify();
		props.equip();
		Perk.onSpawn(bpPlayer);
		CharacterType ct = props.getCharacterType();
		if (ct == null) {
			bpPlayer.teleport(this.characterSelectionLocation);
			return;
		}
		Location spawnLoc = this.getSpawnLocation(team);
		bpPlayer.teleport(spawnLoc);
	}

	@Override
	protected void saveExtra(YamlConfiguration yml) {
		yml.set(this.getName() + ".charSelLoc",
				(Object) (this.characterSelectionLocation.getWorld().getName() + ","
						+ this.characterSelectionLocation.getX() + "," + this.characterSelectionLocation.getY() + ","
						+ this.characterSelectionLocation.getZ() + "," + this.characterSelectionLocation.getYaw() + ","
						+ this.characterSelectionLocation.getPitch()));
		LinkedList<ClanChallenge> days = this.scheduler.getDays();
		LinkedList<String> rawDays = new LinkedList<String>();
		for (ClanChallenge day : days) {
			rawDays.add(day.serialize());
		}
		yml.set(this.getName() + ".schedule", rawDays);
	}

	public boolean canBegin() {
		Configuration config;
		int hour = Calendar.getInstance().get(11);
		return hour >= (config = Breakpoint.getBreakpointConfig()).getCWBeginHour() && hour < config.getCWEndHour()
				&& this.day != null;
	}

	public static boolean hasToEnd() {
		Configuration config = Breakpoint.getBreakpointConfig();
		return Calendar.getInstance().get(11) >= config.getCWEndHour();
	}

	public void broadcastGlobally(String message) {
		if (this.day == null) {
			throw new IllegalArgumentException("day == null");
		}
		Breakpoint.broadcast(MessageType.CW_BROADCAST_PREFIX.getTranslation().getValue(this.getName(),
				this.day.getChallengingClan().getColoredName(), this.day.getChallengedClan().getColoredName(),
				this.day.getMaximumPlayers()) + " " + (Object) ChatColor.GOLD + message);
	}

	public void setDay() {
		this.day = this.scheduler.getCurrentDay();
	}

	public ClanChallenge getDay() {
		return this.day;
	}

	public void setDay(ClanChallenge day) {
		this.day = day;
	}

	public CWScheduler getScheduler() {
		return this.scheduler;
	}

	public boolean hasBegun() {
		return this.begun;
	}

	public void setBegun(boolean begun) {
		this.begun = begun;
	}

	public int[] getWins() {
		return this.wins;
	}

	public int getRound() {
		return this.round;
	}

	public void setRound(int round) {
		this.round = round;
	}
}
