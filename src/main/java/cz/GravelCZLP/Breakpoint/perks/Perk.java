/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.google.common.base.Preconditions
 *  javax.annotation.Nonnull
 *  org.apache.commons.lang.Validate
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.entity.Player
 *  org.bukkit.event.entity.EntityDamageByEntityEvent
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.material.MaterialData
 */
package cz.GravelCZLP.Breakpoint.perks;

import com.google.common.base.Preconditions;
import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.perks.PerkType;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nonnull;
import me.limeth.storageAPI.Column;
import me.limeth.storageAPI.ColumnType;
import me.limeth.storageAPI.Storage;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

public class Perk {
	private PerkType type;
	private int livesLeft;
	private boolean enabled;
	public static final String MENU_TITLE = "" + (Object) ChatColor.DARK_PURPLE + (Object) ChatColor.BOLD
			+ "BREAKPOINT " + (Object) ChatColor.RESET + "PERKY";

	public Perk(@Nonnull PerkType type, int livesLeft, boolean enabled) {
		Validate.notNull((Object) ((Object) type), (String) "Type cannot be null!");
		this.type = type;
		this.livesLeft = livesLeft;
		this.enabled = enabled;
	}

	public static void onMove(BPPlayer player, PlayerMoveEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onMove(event);
		}
	}

	public static void onSpawn(BPPlayer player) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onSpawn(player);
		}
	}

	public static void onDamageDealtByEntity(BPPlayer player, EntityDamageByEntityEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onDamageDealtByEntity(event);
		}
	}

	public static void onDamageDealtByProjectile(BPPlayer player, EntityDamageByEntityEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onDamageDealtByProjectile(event);
		}
	}

	public static void onDamageDealtByPlayer(BPPlayer player, EntityDamageByEntityEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onDamageDealtByPlayer(event);
		}
	}

	public static void onDamageTakenFromEntity(BPPlayer player, EntityDamageByEntityEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onDamageTakenFromEntity(event);
		}
	}

	public static void onDamageTakenFromProjectile(BPPlayer player, EntityDamageByEntityEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onDamageTakenFromProjectile(event);
		}
	}

	public static void onDamageTakenFromPlayer(BPPlayer player, EntityDamageByEntityEvent event) {
		for (Perk perk : player.getPerks()) {
			if (!perk.isEnabled())
				continue;
			perk.getType().onDamageTakenFromPlayer(event);
		}
	}

	public String serialize() {
		return this.type.name() + "," + this.livesLeft + "," + this.enabled;
	}

	public static Perk deserialize(String serialized) {
		String[] array = serialized.split(",");
		PerkType perk = PerkType.valueOf(array[0]);
		int livesLeft = Integer.parseInt(array[1]);
		boolean equipped = Boolean.parseBoolean(array[2]);
		return new Perk(perk, livesLeft, equipped);
	}

	public ItemStack buildItemStack() {
		MaterialData md = this.type.getMaterialData();
		ItemStack is = new ItemStack(md.getItemType(), 1, (short) md.getData());
		ItemMeta im = is.getItemMeta();
		LinkedList<String> lore = new LinkedList<String>();
		MessageType actionMT = this.enabled ? MessageType.MENU_PERKS_DISABLE : MessageType.MENU_PERKS_ENABLE;
		String name = actionMT.getTranslation().getValue(this.type.getName());
		lore.add(MessageType.MENU_PERKS_LIVESLEFT.getTranslation().getValue("" + this.livesLeft + ""));
		lore.addAll(this.type.getDescription());
		im.setLore(lore);
		im.setDisplayName(name);
		is.setItemMeta(im);
		return is;
	}

	public boolean hasExpired() {
		return this.livesLeft <= 0;
	}

	public int decreaseLivesLeft(int amount) {
		return this.livesLeft -= amount;
	}

	public int increaseLivesLeft(int amount) {
		return this.livesLeft += amount;
	}

	public int decreaseLivesLeft() {
		return this.decreaseLivesLeft(1);
	}

	public int increaseLivesLeft() {
		return this.increaseLivesLeft(1);
	}

	public PerkType getType() {
		return this.type;
	}

	public void setType(@Nonnull PerkType type) {
		Preconditions.checkNotNull((Object) ((Object) type), (Object) "Type cannot be null!");
		this.type = type;
	}

	public int getLivesLeft() {
		return this.livesLeft;
	}

	public void setLivesLeft(int livesLeft) {
		this.livesLeft = livesLeft;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static List<Perk> loadPlayerPerks(Storage storage) throws Exception {
		LinkedList<Perk> perks = new LinkedList<Perk>();
		List<String> list = storage.getList("perks", String.class, new LinkedList());
		if (list == null) {
			return perks;
		}
		for (String serialized : list) {
			try {
				Perk perk = Perk.deserialize(serialized);
				perks.add(perk);
			} catch (Exception e) {
				Breakpoint.warn("Error when deserializing a perk: " + e.getMessage() + " - " + serialized);
			}
		}
		return perks;
	}

	public static void savePlayerPerks(Storage storage, List<Perk> perks) {
		LinkedList<String> list = new LinkedList<String>();
		for (Perk perk : perks) {
			if (perk == null || perk.hasExpired())
				continue;
			String serialized = perk.serialize();
			list.add(serialized);
		}
		storage.put("perks", list);
	}

	public static List<Column> getRequiredMySQLColumns() {
		return Arrays.asList(new Column("perks", ColumnType.VARCHAR, 256, new String[0]));
	}

	public static void onMenuClick(InventoryClickEvent event, BPPlayer bpPlayer) {
		event.setCancelled(true);
		Player player = bpPlayer.getPlayer();
		List<Perk> perks = bpPlayer.getPerks();
		int slotId = event.getRawSlot();
		Perk perk = Perk.getPerkAt(slotId, perks);
		if (perk != null) {
			if (perk.isEnabled()) {
				Inventory inv = event.getInventory();
				perk.setEnabled(false);
				Perk.equipMenu(bpPlayer, inv);
			} else {
				int max;
				int enabled = bpPlayer.getEnabledPerks().size();
				if (enabled < (max = bpPlayer.getMaxEquippedPerks())) {
					Inventory inv = event.getInventory();
					perk.setEnabled(true);
					Perk.equipMenu(bpPlayer, inv);
				} else {
					player.sendMessage(MessageType.MENU_PERKS_FULL_VIP.getTranslation().getValue(max));
				}
			}
		}
		InventoryMenuManager.updateInventoryDelayed(player);
	}

	public static InventoryView showPerkMenu(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		List<Perk> perks = bpPlayer.getPerks();
		if (perks.size() <= 0) {
			player.sendMessage(MessageType.MENU_PERKS_EMPTY.getTranslation().getValue(new Object[0]));
			return null;
		}
		int rows = bpPlayer.getPerkInventoryRows();
		Inventory inv = Bukkit.getServer().createInventory((InventoryHolder) player, 9 * rows, MENU_TITLE);
		Perk.equipMenu(bpPlayer, inv);
		player.closeInventory();
		return player.openInventory(inv);
	}

	public static void equipMenu(BPPlayer bpTarget, Inventory inv) {
		int i;
		List<Perk> perks = bpTarget.getPerks();
		int size = inv.getSize();
		int borderStart = (int) Math.ceil((double) bpTarget.getDisabledPerks().size() / 9.0) * 9;
		inv.clear();
		for (i = 0; i < 9; ++i) {
			inv.setItem(borderStart + i, InventoryMenuManager.getBorder());
		}
		for (i = 0; i < size; ++i) {
			Perk perk = Perk.getPerkAt(i, perks);
			if (perk == null)
				continue;
			ItemStack is = perk.buildItemStack();
			inv.setItem(i, is);
		}
	}

	public static Perk getPerkAt(int slotId, List<Perk> perks) {
		if (slotId < 0) {
			return null;
		}
		LinkedList<Perk> enabledPerks = new LinkedList<Perk>();
		LinkedList<Perk> disabledPerks = new LinkedList<Perk>();
		for (Perk perk : perks) {
			if (perk.isEnabled()) {
				enabledPerks.add(perk);
				continue;
			}
			disabledPerks.add(perk);
		}
		int disabled = disabledPerks.size();
		int enabled = enabledPerks.size();
		int disabledRows = (int) Math.ceil((double) disabled / 9.0);
		int enabledStart = 9 * (disabledRows + 1);
		if (slotId < disabled) {
			return (Perk) disabledPerks.get(slotId);
		}
		if (slotId >= enabledStart && slotId < enabledStart + enabled) {
			return (Perk) enabledPerks.get(slotId - enabledStart);
		}
		return null;
	}
}
