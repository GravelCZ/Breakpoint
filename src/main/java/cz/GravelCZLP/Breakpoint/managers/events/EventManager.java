/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.event.player.PlayerInteractEvent
 */
package cz.GravelCZLP.Breakpoint.managers.events;

import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.event.player.PlayerInteractEvent;

public interface EventManager {
	public void showLobbyMenu(BPPlayer var1);

	public void onPlayerInteract(PlayerInteractEvent var1);

	public void save();
}
