/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Chunk
 *  org.bukkit.Color
 *  org.bukkit.GameMode
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.Sound
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.block.BlockFace
 *  org.bukkit.block.BlockState
 *  org.bukkit.block.Sign
 *  org.bukkit.entity.EnderCrystal
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.EntityType
 *  org.bukkit.entity.HumanEntity
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.ItemFrame
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.Listener
 *  org.bukkit.event.block.Action
 *  org.bukkit.event.block.BlockBreakEvent
 *  org.bukkit.event.block.BlockBurnEvent
 *  org.bukkit.event.block.BlockIgniteEvent
 *  org.bukkit.event.block.BlockIgniteEvent$IgniteCause
 *  org.bukkit.event.block.BlockPlaceEvent
 *  org.bukkit.event.block.BlockSpreadEvent
 *  org.bukkit.event.entity.EntityDeathEvent
 *  org.bukkit.event.entity.EntityExplodeEvent
 *  org.bukkit.event.entity.FoodLevelChangeEvent
 *  org.bukkit.event.hanging.HangingBreakByEntityEvent
 *  org.bukkit.event.hanging.HangingBreakEvent
 *  org.bukkit.event.hanging.HangingBreakEvent$RemoveCause
 *  org.bukkit.event.inventory.InventoryOpenEvent
 *  org.bukkit.event.inventory.InventoryType
 *  org.bukkit.event.player.PlayerBedEnterEvent
 *  org.bukkit.event.player.PlayerInteractEntityEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerItemDamageEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerPickupItemEvent
 *  org.bukkit.event.player.PlayerTeleportEvent
 *  org.bukkit.event.player.PlayerToggleFlightEvent
 *  org.bukkit.event.world.ChunkLoadEvent
 *  org.bukkit.event.world.ChunkUnloadEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 *  org.bukkit.util.Vector
 */
package cz.GravelCZLP.Breakpoint.listeners;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.achievements.Achievement;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.game.MapPoll;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.FlagManager;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.managers.AbilityManager;
import cz.GravelCZLP.Breakpoint.managers.DoubleMoneyManager;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import cz.GravelCZLP.Breakpoint.managers.PlayerManager;
import cz.GravelCZLP.Breakpoint.managers.ShopManager;
import cz.GravelCZLP.Breakpoint.perks.Perk;
import cz.GravelCZLP.Breakpoint.perks.PerkType;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.CooldownType;
import cz.GravelCZLP.Breakpoint.players.Settings;

public class PlayerInteractListener implements Listener {
	Breakpoint plugin;

	public PlayerInteractListener(Breakpoint p) {
		this.plugin = p;
	}

	@SuppressWarnings("deprecation")
	public void onPlayerUseItem(PlayerInteractEvent event) {
		Action action = event.getAction();
		if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
			final Player player = event.getPlayer();
			final BPPlayer bpPlayer = BPPlayer.get(player);
			ItemStack item = player.getInventory().getItemInMainHand();
			Material mat = item.getType();
			short durability = item.getDurability();
			if (bpPlayer.isInGame()) {
				Game game = bpPlayer.getGame();
				if (game.votingInProgress()) {
					this.voting(event, bpPlayer);
				}
				if (mat == Material.INK_SACK && durability == 1) {
					if (player.getHealth() < 20.0 && !bpPlayer.hasCooldown(CooldownType.HEAL.getPath(), 0.5, true)) {
						Location loc = player.getLocation();
						World world = loc.getWorld();
						player.setItemInHand(PlayerManager.decreaseItem(item));
						if (player.hasPotionEffect(PotionEffectType.HEAL)) {
							player.removePotionEffect(PotionEffectType.HEAL);
						}
						player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 1, 2), true);
						world.playSound(loc, Sound.ENTITY_GENERIC_EAT, 1.0f, 1.0f);
					}
				} else if (mat == Material.SPLASH_POTION && bpPlayer.isInGame()) {
					PotionMeta pm = (PotionMeta) item.getItemMeta();
					List<PotionEffect> pe = pm.getCustomEffects();
					int type = 0;
					for (PotionEffect pef : pe) {
						if (pef.getType() == PotionEffectType.HARM) {
							type = 1;
						} else if (pef.getType() == PotionEffectType.POISON) {
							type = 2;
						} else if (pef.getType() == PotionEffectType.BLINDNESS || pef.getType() == PotionEffectType.CONFUSION) {
							type = 3;
						}
					}
					if (!bpPlayer.hasCooldown(CooldownType.POTION_RAW.getPath() + type, 0.85, true)) {
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								player.getInventory().setItem(player.getInventory().getHeldItemSlot(), item.clone());
							}
						}, 17L);
					} else {
						event.setCancelled(true);
						player.updateInventory();
					}
				} else {
					game.getListener().onPlayerRightClickItem(event, bpPlayer, item);
				}
			} else if (bpPlayer.isInLobby()) {
				if (mat == Material.NAME_TAG) {
					GameMode gamemode = player.getGameMode();
					if (gamemode != GameMode.CREATIVE) {
						event.setCancelled(true);
						bpPlayer.setAchievementViewTarget(bpPlayer);
						bpPlayer.setAchievementViewPage(0);
						Achievement.showAchievementMenu(bpPlayer);
					}
				} else if (mat == Material.REDSTONE_COMPARATOR) {
					GameMode gamemode = player.getGameMode();
					if (gamemode != GameMode.CREATIVE) {
						event.setCancelled(true);
						Settings.showSettingsMenu(bpPlayer);
					}
				} else if (mat == Material.EXP_BOTTLE &&  player.getGameMode() != GameMode.CREATIVE) {
					event.setCancelled(true);
					Perk.showPerkMenu(bpPlayer);
					bpPlayer.getPlayer().updateInventory();
				}
			}
		} else if (action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK) {
			Player player = event.getPlayer();
			BPPlayer bpPlayer = BPPlayer.get(player);
			ItemStack item = player.getItemInHand();
			Material mat = item.getType();
			if (mat == Material.BLAZE_ROD) {
				if (!bpPlayer.hasCooldown(CooldownType.BLAZE_ROD_MAGE.getPath(), 2.0, true)) {
					Location eyeLoc = player.getEyeLocation();
					AbilityManager.launchFireball(player, eyeLoc, AbilityManager.getDirection(player));
				}
			} else if (mat == Material.STICK) {
				if (!bpPlayer.hasCooldown(CooldownType.STICK_MAGE.getPath(), 2.0, true)) {
					Location eyeLoc = player.getEyeLocation();
					AbilityManager.launchSmallFireball(player, eyeLoc, AbilityManager.getDirection(player));
				}
			} else if (mat == Material.FEATHER) {
				if (!bpPlayer.hasCooldown(CooldownType.FEATHER_MAGE.getPath(), 3.0, true)) {
					AbilityManager.launchPlayer(player);
				}
			} else {
				Game game = bpPlayer.getGame();
				if (game != null) {
					game.getListener().onPlayerLeftClickItem(event, bpPlayer, item);
				}
			}
		}
	}

	public void voting(PlayerInteractEvent event, BPPlayer bpPlayer) {
		String playerName;
		int mapId;
		PlayerInventory inv;
		Game game = bpPlayer.getGame();
		Player player = bpPlayer.getPlayer();
		MapPoll mapPoll = game.getMapPoll();
		if (mapPoll.isIdCorrect(mapId = (inv = player.getInventory()).getHeldItemSlot())
				&& !mapPoll.hasVoted(playerName = player.getName())) {
			boolean b = player.hasPermission("Breakpoint.game.votetwo");
			int strength = b ? 2 : 1;
			mapPoll.vote(playerName, mapId, strength);
			PlayerManager.clearHotBar(inv);
			player.updateInventory();
			String mapName = mapPoll.maps[mapId];
			player.sendMessage(MessageType.VOTING_VOTE.getTranslation().getValue(mapName));
		}
	}

	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		if (entity instanceof ItemFrame && !event.getPlayer().hasPermission("Breakpoint.interact")) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (this.plugin.hasEvent()) {
			this.plugin.evtm.onPlayerInteract(event);
		}
		if (!(this.onPlayerBlockInteract(event))) {
			return;
		}
		this.onPlayerUseItem(event);
	}

	public boolean onPlayerBlockInteract(PlayerInteractEvent event) {
		Block block;
		Action action = event.getAction();
		if (action == Action.RIGHT_CLICK_BLOCK) {
			Player player = event.getPlayer();
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (bpPlayer.isInGame()) {
				Game game = bpPlayer.getGame();
				game.getListener().onPlayerRightClickBlock(event, bpPlayer);
			} else {
				Block block2 = event.getClickedBlock();
				Material type = block2.getType();
				if (type == Material.WALL_SIGN || type == Material.SIGN_POST) {
					Sign sign = (Sign) block2.getState();
					Location loc = block2.getLocation();
					Game clickedGame = GameManager.getGame(loc);
					if (clickedGame != null) {
						if (clickedGame.isPlayable()) {
							try {
								clickedGame.join(bpPlayer);
							} catch (Exception e) {
								player.sendMessage(e.getMessage());
							}
						} else {
							player.sendMessage(
									MessageType.LOBBY_GAME_NOTREADY.getTranslation().getValue(clickedGame.getName()));
						}
					} else {
						String[] lines = sign.getLines();
						if (ShopManager.isShop(lines)) {
							ShopManager.buyItem(bpPlayer, sign, lines);
						}
					}
					event.setCancelled(true);
					return false;
				}
			}
		} else if (action == Action.PHYSICAL
				&& (block = event.getClickedBlock()).getType() == Material.STONE_PLATE) {
			Block below = block.getRelative(BlockFace.DOWN);
			Material belowMat = below.getType();
			Player player = event.getPlayer();
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (!bpPlayer.isInGame()) {
				Configuration config = Breakpoint.getBreakpointConfig();
				if (belowMat == Material.EMERALD_BLOCK) {
					bpPlayer.teleport(config.getShopLocation());
				} else if (belowMat == Material.QUARTZ_BLOCK) {
					bpPlayer.teleport(config.getLobbyLocation());
				} else if (belowMat == Material.REDSTONE_BLOCK) {
					bpPlayer.teleport(config.getStaffListLocation());
				} else if (belowMat == Material.GOLD_BLOCK) {
					bpPlayer.teleport(config.getVipInfoLocation());
				}
			} else {
				Game game = bpPlayer.getGame();
				game.getListener().onPlayerPhysicallyInteractWithBlock(event, bpPlayer, below);
			}
		}
		return true;
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("Breakpoint.build") || player.getGameMode() != GameMode.CREATIVE) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("Breakpoint.build") || player.getGameMode() != GameMode.CREATIVE) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event) {
		event.setCancelled(true);
		Location loc = event.getLocation();
		World world = loc.getWorld();
		world.createExplosion(loc.getX(), loc.getY(), loc.getZ(), event.getYield(), false, false);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		ItemStack item;
		BPPlayer bpPlayer = BPPlayer.get(e.getPlayer());
		if (bpPlayer.isInGame() && bpPlayer.getGame().getType() == GameType.CTF
				&& (item = e.getItem().getItemStack()).getType() == Material.SPECKLED_MELON
				&& item.getItemMeta().getDisplayName() == "melounBoost") {
			e.setCancelled(true);
			e.getItem().remove();
			e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_GENERIC_EAT, 1.0f, 2.0f);
			Color color = Color.WHITE;
			CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
			color = props.getTeam().getColor();
			e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 60, 3, false, true, color));
		}
	}

	@EventHandler
	public void onHangingBreak(HangingBreakByEntityEvent event) {
		if (event.getCause() == HangingBreakEvent.RemoveCause.ENTITY) {
			if (event.getRemover() instanceof Player) {
				Player p = (Player) event.getRemover();
				BPPlayer bpPlayer = BPPlayer.get(p);
				if (!bpPlayer.isInLobby() && !p.hasPermission("Breakpoint.interact")) {
					event.setCancelled(true);
				}
			} else {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event) {
		Inventory inv = event.getInventory();
		BPPlayer bpPlayer = BPPlayer.get(event.getPlayer().getName());
		if (bpPlayer.isInGame()) {
			return;
		}
		if (bpPlayer.isInLobby() && inv.getType() != InventoryType.CHEST) {
			return;
		}
	}

	@EventHandler
	public void onPlayerBedEnter(PlayerBedEnterEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onBlockBurn(BlockBurnEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onBlockSpread(BlockSpreadEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onBlockIgnite(BlockIgniteEvent event) {
		BlockIgniteEvent.IgniteCause ic = event.getCause();
		if (ic != BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		BPPlayer bpPlayer = BPPlayer.get(player);
		Game game = bpPlayer.getGame();
		if (game != null) {
			game.getListener().onPlayerTeleport(event, bpPlayer);
		}
	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent e) {
		for (int i = 0; i < e.getChunk().getEntities().length; ++i) {
			if (e.getChunk().getEntities()[i].getType() != EntityType.ENDER_CRYSTAL)
				continue;
			e.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onChunkLoad(ChunkLoadEvent e) {
		for (Entity ent : e.getChunk().getEntities()) {
			if (!(ent instanceof Item) || ((Item) ent).getItemStack().getType() != Material.SPECKLED_MELON
					|| DoubleMoneyManager.isDoubleXP())
				continue;
			ent.remove();
		}
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		BPPlayer bpPlayer = BPPlayer.get(e.getPlayer());
		Game game = bpPlayer.getGame();
		if (game != null) {
			game.getListener().onPlayerMove(bpPlayer, e.getFrom(), e.getTo(), e);
		}
	}

	@EventHandler
	public void onEnderCrystalDie(EntityDeathEvent e) {
		if (e.getEntityType() == EntityType.ENDER_CRYSTAL) {
			EnderCrystal crystal = (EnderCrystal) e.getEntity();
			for (Game g : GameManager.getGames()) {
				FlagManager flm = ((CTFGame) g).getFlagManager();
				if (g.getType() != GameType.CTF || !flm.isTeamFlag(crystal))
					continue;
				Team t = flm.getFlagTeam(crystal);
				flm.spawnFlag(flm.getFlagLocation(t), t);
			}
		}
	}

	@EventHandler
	public void onPlayerToggleFlight(PlayerToggleFlightEvent e) {
		Player p = e.getPlayer();
		BPPlayer bpPlayer = BPPlayer.get(p);
		if (p.getGameMode() != GameMode.CREATIVE && bpPlayer.isInGame() && bpPlayer.getPerk(PerkType.AIRBORN) != null
				&& bpPlayer.getPerk(PerkType.AIRBORN).isEnabled()) {
			e.setCancelled(true);
			p.setAllowFlight(false);
			p.setFlying(false);
			p.setVelocity(p.getLocation().getDirection().multiply(2.5).setY(0.9));
			p.playSound(p.getLocation(), Sound.ENTITY_BLAZE_SHOOT, 1.0f, 1.0f);
			AbilityManager.showJumpEffect(p.getLocation(), 2);
		}
	}

	@EventHandler
	public void onItemDamage(PlayerItemDamageEvent e) {
		Player p = e.getPlayer();
		BPPlayer bpPlayer = BPPlayer.get(p);
		if (bpPlayer.isInGame()) {
			e.setCancelled(true);
		}
	}

}
