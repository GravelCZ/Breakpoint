/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapFont
 *  org.bukkit.map.MapFont$CharacterSprite
 *  org.bukkit.map.MapPalette
 *  org.bukkit.map.MapRenderer
 *  org.bukkit.map.MapView
 *  org.bukkit.map.MinecraftFont
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import javax.imageio.ImageIO;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapFont;
import org.bukkit.map.MapPalette;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;

public abstract class BPMapRenderer extends MapRenderer {
	public static final int MAP_SIZE = 128;
	public static final char COLOR_CHAR = '\u00a7';

	public void set(MapView mw) {
		try {
			if (mw.getRenderers() != null) {
				for (MapRenderer mr : mw.getRenderers()) {
					mw.removeRenderer(mr);
				}
			}
			mw.addRenderer((MapRenderer) this);
		} catch (NullPointerException nullPointerException) {
			// empty catch block
		}
	}

	public static void setBackgroundColor(MapCanvas canvas, byte color) {
		for (int y = 0; y < 128; ++y) {
			for (int x = 0; x < 128; ++x) {
				canvas.setPixel(x, y, color);
			}
		}
	}

	public static void drawRectangle(MapCanvas canvas, int startX, int startY, int width, int height, byte color) {
		for (int x = startX; x < startX + width; ++x) {
			for (int y = startY; y < startY + height; ++y) {
				canvas.setPixel(x, y, color);
			}
		}
	}

	public static void drawRectangleFade(MapCanvas canvas, int startX, int startY, int width, int height, byte[] color,
			int fadeId) {
		int colorLength = color.length + color.length - 1;
		double barHeight = height / colorLength;
		for (double bar = 0.0; bar < (double) colorLength; bar += 1.0) {
			int x;
			int y;
			boolean fade;
			int barStartY = (int) (barHeight * bar);
			int reminder = (int) bar % 2;
			boolean bl = fade = reminder != 0;
			if (fade) {
				for (x = startX; x < startX + width; ++x) {
					for (y = startY; y < startY + height; ++y) {
						int color1 = (int) (bar - (double) reminder) / 2;
						int color2 = (int) (bar - (double) reminder) / 2 + 1;
						int curColor = (x + y + barStartY) % 2 == fadeId ? color1 : color2;
						canvas.setPixel(x, y + barStartY, color[curColor]);
					}
				}
				continue;
			}
			for (x = startX; x < startX + width; ++x) {
				for (y = startY; y < startY + height; ++y) {
					canvas.setPixel(x, y + barStartY, color[(int) (bar / 2.0)]);
				}
			}
		}
	}

	public void drawAvailableColors(MapCanvas canvas) {
		for (int y = 0; y < 32; ++y) {
			for (int x = 0; x < 32; ++x) {
				int id = y * 32 + x;
				try {
					BPMapRenderer.drawRectangle(canvas, x * 4, y * 4, 4, 4,
							BPMapPalette.matchColor(BPMapPalette.colors[id]));
					continue;
				} catch (Exception e) {
					return;
				}
			}
		}
	}

	public void drawText(MapCanvas canvas, int x, int y, int width, int height, MapFont font, String text, byte color) {
		int xStart = x;
		if (!font.isValid(text)) {
			throw new IllegalArgumentException("text contains invalid characters");
		}
		for (int i = 0; i < text.length(); ++i) {
			int j;
			char ch = text.charAt(i);
			if (ch == '\n') {
				x = xStart;
				y += font.getHeight() + 1;
				continue;
			}
			if (ch == '\u00a7' && (j = text.indexOf(59, i)) >= 0) {
				try {
					color = Byte.parseByte(text.substring(i + 1, j));
					i = j;
					continue;
				} catch (NumberFormatException numberFormatException) {
					// empty catch block
				}
			}
			MapFont.CharacterSprite sprite = font.getChar(text.charAt(i));
			for (int r = 0; r < font.getHeight(); ++r) {
				for (int c = 0; c < sprite.getWidth(); ++c) {
					for (int xx = 0; xx < width; ++xx) {
						for (int yy = 0; yy < height; ++yy) {
							if (!sprite.get(r, c))
								continue;
							canvas.setPixel(x + xx + c * width, y + yy + r * height, color);
						}
					}
				}
			}
			x += (sprite.getWidth() + 1) * width;
		}
	}

	public void drawText(Byte[][] canvas, int x, int y, int width, int height, MapFont font, String text, byte color) {
		int xStart = x;
		if (!font.isValid(text)) {
			throw new IllegalArgumentException("text contains invalid characters");
		}
		for (int i = 0; i < text.length(); ++i) {
			int j;
			char ch = text.charAt(i);
			if (ch == '\n') {
				x = xStart;
				y += font.getHeight() + 1;
				continue;
			}
			if (ch == '\u00a7' && (j = text.indexOf(59, i)) >= 0) {
				try {
					color = Byte.parseByte(text.substring(i + 1, j));
					i = j;
					continue;
				} catch (NumberFormatException numberFormatException) {
					// empty catch block
				}
			}
			MapFont.CharacterSprite sprite = font.getChar(text.charAt(i));
			for (int r = 0; r < font.getHeight(); ++r) {
				for (int c = 0; c < sprite.getWidth(); ++c) {
					for (int xx = 0; xx < width; ++xx) {
						for (int yy = 0; yy < height; ++yy) {
							if (!sprite.get(r, c))
								continue;
							int absX = x + xx + c * width;
							int absY = y + yy + r * height;
							if (canvas.length <= absX || canvas[absX].length <= absY)
								continue;
							canvas[absX][absY] = color;
						}
					}
				}
			}
			x += (sprite.getWidth() + 1) * width;
		}
	}

	public static void drawTextOld(MapCanvas canvas, int x, int y, int width, int height, MapFont font, String text) {
		int xStart = x;
		byte color = 44;
		if (!MinecraftFont.Font.isValid(text)) {
			return;
		}
		for (int i = 0; i < text.length(); ++i) {
			int j;
			char ch = text.charAt(i);
			if (ch == '\n') {
				x = xStart;
				y += font.getHeight() + 1;
				continue;
			}
			if (ch == '\u00a7' && (j = text.indexOf(59, i)) >= 0) {
				try {
					color = Byte.parseByte(text.substring(i + 1, j));
					i = j;
					continue;
				} catch (NumberFormatException numberFormatException) {
					// empty catch block
				}
			}
			MapFont.CharacterSprite sprite = font.getChar(text.charAt(i));
			for (int r = 0; r < font.getHeight(); ++r) {
				for (int c = 0; c < sprite.getWidth(); ++c) {
					for (int xx = 0; xx < width; ++xx) {
						for (int yy = 0; yy < height; ++yy) {
							if (!sprite.get(r, c))
								continue;
							canvas.setPixel(x + xx + c * width, y + yy + r * height, color);
						}
					}
				}
			}
			x += (sprite.getWidth() + 1) * width;
		}
	}

	public static void drawTextOld(Byte[][] canvas, int x, int y, int width, int height, MapFont font, String text) {
		int xStart = x;
		byte color = 44;
		if (!MinecraftFont.Font.isValid(text)) {
			return;
		}
		for (int i = 0; i < text.length(); ++i) {
			int j;
			char ch = text.charAt(i);
			if (ch == '\n') {
				x = xStart;
				y += font.getHeight() + 1;
				continue;
			}
			if (ch == '\u00a7' && (j = text.indexOf(59, i)) >= 0) {
				try {
					color = Byte.parseByte(text.substring(i + 1, j));
					i = j;
					continue;
				} catch (NumberFormatException numberFormatException) {
					// empty catch block
				}
			}
			MapFont.CharacterSprite sprite = font.getChar(text.charAt(i));
			for (int r = 0; r < font.getHeight(); ++r) {
				for (int c = 0; c < sprite.getWidth(); ++c) {
					for (int xx = 0; xx < width; ++xx) {
						for (int yy = 0; yy < height; ++yy) {
							if (!sprite.get(r, c))
								continue;
							int absX = x + xx + c * width;
							int absY = y + yy + r * height;
							if (canvas.length <= absX || canvas[absX].length <= absY)
								continue;
							canvas[absX][absY] = color;
						}
					}
				}
			}
			x += (sprite.getWidth() + 1) * width;
		}
	}

	public static byte[][] toBytes(BufferedImage image) {
		if (image == null) {
			byte[][] map = new byte[128][128];
			byte color = BPMapPalette.getColor(11, 0);
			for (int x = 0; x < 128; ++x) {
				for (int y = 0; y < 128; ++y) {
					map[x][y] = color;
				}
			}
			return map;
		}
		int width = image.getWidth();
		int height = image.getHeight();
		byte[][] map = new byte[width][height];
		for (int x = 0; x < width; ++x) {
			for (int y = 0; y < height; ++y) {
				Color color = Color.decode(Integer.toString(image.getRGB(x, y)));
				map[x][y] = MapPalette.matchColor((Color) color);
			}
		}
		return map;
	}

	public static int getWidth(String s) {
		if (s.length() == 0) {
			return MinecraftFont.Font.getWidth(s);
		}
		return MinecraftFont.Font.getWidth(s) + s.length() - 1;
	}

	public static void drawBytes(MapCanvas canvas, byte[][] image) {
		for (int x = 0; x < image.length; ++x) {
			for (int y = 0; y < image[0].length; ++y) {
				canvas.setPixel(x, y, image[x][y]);
			}
		}
	}

	public static BufferedImage getImage(String path) {
		try {
			File file = new File(path);
			if (!file.exists()) {
				return null;
			}
			BufferedImage img = ImageIO.read(file);
			return img;
		} catch (Throwable e) {
			Breakpoint.warn("Error when loading map image '" + path + "'.");
			e.printStackTrace();
			return null;
		}
	}
}
