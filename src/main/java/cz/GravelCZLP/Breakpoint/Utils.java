/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  net.minecraft.server.v1_12_R1.MinecraftServer
 *  org.apache.commons.lang3.text.WordUtils
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.World
 *  org.bukkit.craftbukkit.v1_12_R1.CraftServer
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.BreakpointInfo;
import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.managers.StatisticsManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.statistics.TotalPlayerStatistics;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import net.minecraft.server.v1_12_R1.MinecraftServer;
import org.apache.commons.lang3.text.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Utils {
	public static List<Location> getCircle(Location center, double radius, int amount) {
		World w = center.getWorld();
		double inc = 6.283185307179586 / (double) amount;
		ArrayList<Location> locs = new ArrayList<Location>();
		for (int i = 0; i < amount; ++i) {
			double angle = (double) i * inc;
			double x = center.getX() + radius * Math.cos(angle);
			double z = center.getZ() + radius * Math.sin(angle);
			Location loc = new Location(w, x, center.getY(), z);
			locs.add(loc);
		}
		return locs;
	}

	public static class TPSCounter implements Runnable {
		private long lastPoll = System.nanoTime();
		private static final LinkedList<Double> history = new LinkedList();
		private final int tickInterval = 50;

		@Override
		public void run() {
			double tps;
			long startTime = System.nanoTime();
			long timeSpent = (startTime - this.lastPoll) / 1000L;
			if (timeSpent == 0L) {
				timeSpent = 1L;
			}
			if (history.size() > 10) {
				history.remove();
			}
			if ((tps = 5.0E7 / (double) timeSpent) <= 21.0) {
				history.add(tps);
			}
			this.lastPoll = startTime;
		}

		public static double getAvarageTPS() {
			double avg = 0.0;
			for (Double d : history) {
				if (d == null)
					continue;
				avg += d.doubleValue();
			}
			return avg / (double) history.size();
		}
	}

	public static class BPReportPostException extends Exception {
		private static final long serialVersionUID = -1427692136833410347L;

		public BPReportPostException(String s) {
			super(s);
		}
	}

	public static class PastebinReport {
		private static final String DATE = new SimpleDateFormat("dd-MM-yyyy kk:mm Z").format(new Date());
		private static final String API_DEV_KEY = "c5696fc96651b00456d31454d970d53d";
		private StringBuilder report = new StringBuilder();
		private String url = "";

		private void append(String s) {
			this.report.append(s + '\n');
		}

		private String getNMSVersion() {
			MinecraftServer ms = ((CraftServer) Bukkit.getServer()).getServer();
			return ms.getVersion();
		}

		public String getReport() {
			this.createReport();
			this.postReport();
			return this.url;
		}

		private void appendSystemInfo() {
			Runtime runtime = Runtime.getRuntime();
			this.append("AntiCheat Version: " + Breakpoint.getVersion());
			this.append("Server Version: " + Bukkit.getVersion());
			this.append("Server Implementation: " + Bukkit.getName());
			this.append("Server ID: " + Bukkit.getServerId());
			this.append("NMS Version: " + this.getNMSVersion());
			this.append(
					"Java Version: " + System.getProperty("java.vendor") + " " + System.getProperty("java.version"));
			this.append("OS: " + System.getProperty("os.name") + " " + System.getProperty("os.version"));
			this.append("Free Memory: " + runtime.freeMemory() / 1024L / 1024L + "MB");
			this.append("Max Memory: " + runtime.maxMemory() / 1024L / 1024L + "MB");
			this.append("Total Memory: " + runtime.totalMemory() / 1024L / 1024L + "MB");
			this.append("Online Mode: " + Bukkit.getOnlineMode());
			this.append("Players: " + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers());
			this.append("Plugin Count: " + Bukkit.getPluginManager().getPlugins().length);
			this.append("Plugin Uptime: "
					+ (System.currentTimeMillis() - Breakpoint.getInstance().timeBoot) / 1000L / 60L + " minutes");
		}

		private void createReport() {
			this.append("------------ Breakpoint Report - " + DATE + " ------------");
			this.appendSystemInfo();
			TotalPlayerStatistics totplsts = StatisticsManager.getTotalStats();
			this.append("Total kills: " + totplsts.getKills());
			this.append("Average kills: " + totplsts.getAverageKills());
			this.append("Average deaths: " + totplsts.getAverageBought());
			this.append("Average assists: " + totplsts.getAverageAssists());
			this.append("Average flag captures: " + totplsts.getAverageFlagCaptures());
			this.append("Average flag takes: " + totplsts.getAverageFlagTakes());
			this.append("Average money: " + totplsts.getAverageMoney());
			this.append("Average bought: " + totplsts.getAverageBought());
			for (CharacterType ct : CharacterType.values()) {
				String c = WordUtils.capitalize((String) ct.getProperName());
				this.append("Kills " + c + ": " + totplsts.getKills(ct));
			}
			BreakpointInfo info = BreakpointInfo.getActualInfo();
			this.append("Current Players in Lobby" + info.getPlayerIsLobby());
			this.append("Current Players in Game: " + info.getPlayerInGame());
			this.append("Current Players in CTF: " + info.getPlayersInCTF());
			this.append("Current Players in DM: " + info.getPlayersInDM());
			this.append("Best Player in DM: " + info.getBestPlayerInDM());
			this.append("Blue Crystals in CTF: " + info.getBlueCrystals());
			this.append("Red Crystals in CTF: " + info.getRedCrystals());
		}

		public void postReport() {
			try {
				String result;
				URL pastebinAPI = new URL("http://pastebin.com/api/api_post.php");
				HttpsURLConnection conn = (HttpsURLConnection) pastebinAPI.openConnection();
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(5000);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setInstanceFollowRedirects(false);
				conn.setDoOutput(true);
				OutputStream out = conn.getOutputStream();
				StringBuilder post = new StringBuilder();
				post.append("api_option=paste");
				post.append("&api_dev_key=" + this.e(API_DEV_KEY));
				post.append("&api_post_code=" + this.e(this.report.toString()));
				post.append("&api_paste_private=" + this.e("1"));
				post.append("&api_paste_name=" + this.e("Breakpoint Report"));
				post.append("&api_paste_expire_date=" + this.e("1M"));
				post.append("&api_paste_format=" + this.e("text"));
				post.append("&api_user_key=" + this.e(""));
				out.write(post.toString().getBytes());
				out.flush();
				out.close();
				if (conn.getResponseCode() == 200) {
					String line;
					InputStream is = conn.getInputStream();
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					StringBuffer response = new StringBuffer();
					while ((line = br.readLine()) != null) {
						response.append(line);
						response.append("\n\r");
					}
					br.close();
					result = response.toString().trim();
					if (!result.contains("https://") || !result.contains("http://")) {
						throw new BPReportPostException(result);
					}
				} else {
					throw new BPReportPostException("Responce code was other then 200: " + conn.getResponseCode());
				}
				this.url = result.trim();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private String e(String e) throws UnsupportedEncodingException {
			return URLEncoder.encode(e, "utf-8");
		}
	}

	public static class Runnables {
		public static Runnable getPyroSpetialEffect(BPPlayer bpPlayer) {
			if (!bpPlayer.isInGame()) {
				throw new IllegalStateException("Players needs to be in game");
			}
			if (bpPlayer.getGameProperties().getCharacterType() != CharacterType.PYRO) {
				throw new IllegalStateException("Player needs to have Pyro class");
			}
			final Location center = bpPlayer.getPlayer().getLocation();
			Runnable r = new Runnable() {
				double cir1radius = 0.0;
				double cir2radius = 0.0;
				double increseAmount = 0.15;

				@Override
				public void run() {
					if (this.cir1radius <= 1.75) {
						this.cir1radius = this.increseAmount;
					}
					if (this.cir2radius <= 4.75) {
						this.cir2radius = this.increseAmount;
					}
					List<Location> cir1 = Utils.getCircle(center, this.cir1radius, 15);
					List<Location> cir2 = Utils.getCircle(center, this.cir2radius, 30);
					for (Location loc : cir1) {
						loc.getWorld().spawnParticle(Particle.FLAME, loc, 5);
					}
					for (Location loc : cir2) {
						loc.getWorld().spawnParticle(Particle.FLAME, loc, 5);
					}
				}
			};
			return r;
		}

	}

}
