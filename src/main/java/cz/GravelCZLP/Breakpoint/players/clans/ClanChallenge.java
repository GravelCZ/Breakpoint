/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.players.clans;

import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.clans.Clan;
import java.util.Calendar;

public class ClanChallenge {
	private final int dayOfYear;
	private final int maxPlayers;
	private final Clan[] clans = new Clan[2];

	public static ClanChallenge unserialize(String string) {
		String[] values = string.split(",");
		int dayOfYear = Integer.parseInt(values[0]);
		Clan challenging = Clan.get(values[1]);
		Clan challenged = Clan.get(values[2]);
		int maxPlayers = Integer.parseInt(values[3]);
		return new ClanChallenge(dayOfYear, challenging, challenged, maxPlayers);
	}

	public ClanChallenge(int dayOfYear, Clan challenging, Clan challenged, int maxPlayers) {
		if (challenging == null) {
			throw new IllegalArgumentException("challenging == null");
		}
		if (challenged == null) {
			throw new IllegalArgumentException("challenged == null");
		}
		this.dayOfYear = dayOfYear;
		this.maxPlayers = maxPlayers;
		this.clans[0] = challenging;
		this.clans[1] = challenged;
	}

	public boolean isWaiting() {
		return this.dayOfYear > Calendar.getInstance().get(6);
	}

	public String serialize() {
		return "" + this.dayOfYear + "," + this.getChallengingClan().getName() + ","
				+ this.getChallengedClan().getName() + "," + this.maxPlayers;
	}

	public boolean isToday() {
		return Calendar.getInstance().get(6) == this.dayOfYear;
	}

	public int getDayOfWeek() {
		Calendar cal = Calendar.getInstance();
		cal.set(6, this.dayOfYear);
		return cal.get(7);
	}

	public Team getTeam(Clan clan) {
		for (int i = 0; i < 2; ++i) {
			if (!clan.equals(this.clans[i]))
				continue;
			return Team.getById(i);
		}
		return null;
	}

	public Team getTeam(BPPlayer bpPlayer) {
		Clan clan = bpPlayer.getClan();
		if (clan == null) {
			return null;
		}
		return this.getTeam(clan);
	}

	public Clan getChallengingClan() {
		return this.clans[0];
	}

	public Clan getChallengedClan() {
		return this.clans[1];
	}

	public int getDayOfYear() {
		return this.dayOfYear;
	}

	public Clan[] getClans() {
		return this.clans;
	}

	public int getMaximumPlayers() {
		return this.maxPlayers;
	}
}
