/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.GameMode
 *  org.bukkit.Location
 *  org.bukkit.Server
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.command.CommandSender
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.map.MapView
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitScheduler
 *  org.bukkit.scheduler.BukkitTask
 *  org.bukkit.scoreboard.Objective
 */
package cz.GravelCZLP.Breakpoint.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.map.MapView;
import org.bukkit.plugin.Plugin;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.achievements.AchievementType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.managers.PlayerManager;
import cz.GravelCZLP.Breakpoint.managers.SBManager;
import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.CurrentMapRenderer;
import cz.GravelCZLP.Breakpoint.maps.MapManager;
import cz.GravelCZLP.Breakpoint.maps.SizeRenderer;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;

public abstract class Game {
	public static final int defaultMapSeconds = 9 * 60;
	private final GameType type;
	private final GameListener listener;
	private String name;
	private Location signLoc;
	private LinkedList<? extends BPMap> maps;
	protected final List<BPPlayer> players;
	private int activeMapId;
	private int mapSecondsLeft;
	protected final short votingMapId;
	protected final short currentMapMapId;
	protected final short playerAmountRendererMapId;
	private Integer countdownTaskId = null;
	private boolean active;
	private boolean roundEnded;
	private MapPoll mapPoll;
	private final SizeRenderer playerAmountRenderer;
	private final CurrentMapRenderer currentMapRenderer;
	private String firstBloodPlayerName;
	private String lastBloodPlayerName;
	public boolean noPlayers;

	public static final Game loadGame(YamlConfiguration yml, String name) {
		try {
			String rawType = yml.getString(name + ".type");
			GameType type = GameType.valueOf(rawType);
			String[] rawSignLoc = yml.getString(name + ".signLoc", "world,0,0,0").split(",");
			Location signLoc = new Location(Bukkit.getWorld((String) rawSignLoc[0]),
					(double) Integer.parseInt(rawSignLoc[1]), (double) Integer.parseInt(rawSignLoc[2]),
					(double) Integer.parseInt(rawSignLoc[3]));
			return type.loadGame(yml, name, signLoc);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Game(GameType type, String name, Location signLoc, LinkedList<? extends BPMap> maps) {
		if (type == null || name == null || type.getListenerClass() == null || name.length() <= 0 || signLoc == null
				|| maps == null) {
			throw new IllegalArgumentException();
		}
		Class<? extends GameListener> listenerClass = type.getListenerClass();
		try {
			this.listener = listenerClass.getConstructor(Game.class).newInstance(this);
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Cannot create a new instance of GameListener. (Game: " + name + ")");
		}
		this.type = type;
		this.signLoc = signLoc.getBlock().getLocation();
		this.name = name;
		this.maps = maps;
		this.players = new ArrayList<BPPlayer>();
		this.votingMapId = MapManager.getNextFreeId(5);
		this.currentMapMapId = MapManager.getNextFreeId();
		this.currentMapRenderer = new CurrentMapRenderer();
		MapView cmmv = Bukkit.getMap((short) this.currentMapMapId);
		this.currentMapRenderer.set(cmmv);
		this.playerAmountRendererMapId = MapManager.getNextFreeId();
		System.out.println(this.playerAmountRendererMapId);
		this.playerAmountRenderer = new SizeRenderer(BPMapPalette.getColor(13, 0), BPMapPalette.getColor(8, 2), 0);
		MapView rtsmv = Bukkit.getMap((short) this.playerAmountRendererMapId);
		this.playerAmountRenderer.set(rtsmv);
		this.active = this.isPlayable(true);
	}

	public Game(GameType type, String name, Location signLoc) {
		this(type, name, signLoc, new LinkedList());
	}

	public void start() {
		this.changeMap(this.getRandomMapWithCapacity(this.getPlayers().size()));
		this.startCountdown();
		this.startExtra();
		this.setActive(true);
	}

	public abstract void spawn(BPPlayer var1);

	public abstract void reset(BPPlayer var1);

	public abstract void updateProgressObjective(BPPlayer var1);

	public abstract void updateProgressObjectiveHeader(BPPlayer var1);

	public abstract void showInGameMenu(BPPlayer var1);

	protected abstract void endRoundExtra();

	protected abstract void changeMapExtra();

	protected abstract void saveExtra(YamlConfiguration var1);

	protected abstract void startExtra();

	public abstract void onCommand(CommandSender var1, String[] var2);

	public void onPlayerLeaveGame(BPPlayer bpPlayer) {
		this.players.remove(bpPlayer);
		bpPlayer.setGame(null);
		bpPlayer.setGameProperties(null);
		this.updatePlayerAmountRenderer();
		SBManager sbm = bpPlayer.getScoreboardManager();
		SBManager.updateLobbyObjectives();
		sbm.updateSidebarObjective();
		sbm.getProgressObj().unregister();
		sbm.setProgressObj(null);
		Breakpoint.getInstance().getNametagAPIHook().updateNametag(bpPlayer);
	}

	public final void save(YamlConfiguration yml) {
		String mapPath = this.name + ".maps";
		yml.set(this.name, null);
		yml.set(this.name + ".type", (Object) this.type.name());
		yml.set(this.name + ".signLoc", (Object) (this.signLoc.getWorld().getName() + "," + this.signLoc.getBlockX()
				+ "," + this.signLoc.getBlockY() + "," + this.signLoc.getBlockZ()));
		this.saveExtra(yml);
		for (BPMap map : this.maps) {
			if (!map.isPlayable())
				continue;
			map.save(yml, mapPath);
		}
	}

	public void updateLobbyMaps(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		if (Bukkit.getMap((short) this.currentMapMapId) == null
				|| Bukkit.getMap((short) this.playerAmountRendererMapId) == null) {
			return;
		}
		player.sendMap(Bukkit.getMap((short) this.currentMapMapId));
		player.sendMap(Bukkit.getMap((short) this.playerAmountRendererMapId));
	}

	public boolean isPlayable(boolean skipActive) {
		if (!skipActive && !this.isActive()) {
			return false;
		}
		return this.type != null && this.name != null && this.name.length() > 0 && this.signLoc != null
				&& this.getPlayableMaps().size() > 0;
	}

	public boolean isPlayable() {
		return this.isPlayable(false);
	}

	public void join(BPPlayer bpPlayer) throws Exception {
		if (this.noPlayers) {
			return;
		}
		Player player = bpPlayer.getPlayer();
		player.setGameMode(GameMode.ADVENTURE);
		player.setFlying(false);
		player.setAllowFlight(false);
		InventoryMenuManager.saveLobbyMenu(bpPlayer);
		this.players.add(bpPlayer);
		bpPlayer.setGame(this);
		bpPlayer.getPlayer().sendMessage(MessageType.LOBBY_GAME_JOIN.getTranslation().getValue(this.getName()));
		this.updatePlayerAmountRenderer();
		bpPlayer.setPlayerListName();
		player.getInventory().clear();
		SBManager sbm = bpPlayer.getScoreboardManager();
		if (sbm.progressObj == null) {
			sbm.initProgressObj();
		}
		this.updateProgressObjective(bpPlayer);
		sbm.updateSidebarObjective();
		SBManager.updateLobbyObjectives();
	}

	public void updatePlayerAmountRenderer() {
		this.playerAmountRenderer.setSize(this.players.size());
		MapManager.updateMapForNotPlayingPlayers(this.playerAmountRendererMapId);
	}

	public void second() {
		this.setMapSecondsLeft(this.getMapSecondsLeft() - 1);
		if (this.getMapSecondsLeft() > 0) {
			this.scheduleNextSecond();
		} else {
			this.endRound();
			Bukkit.getScheduler().runTaskLater((Plugin) Breakpoint.getInstance(), new Runnable() {

				@Override
				public void run() {
					Game.this.scheduleMapPoll();
				}
			}, 10L);
		}
		this.updateProgressHeaderTime();
	}

	public void updateProgressHeaderTime() {
		for (BPPlayer bpPlayer : this.players) {
			this.updateProgressObjectiveHeader(bpPlayer);
		}
	}

	public void scheduleMapPoll() {
		final Game game = this;
		Bukkit.getScheduler().scheduleSyncDelayedTask((Plugin) Breakpoint.getInstance(), new Runnable() {

			@Override
			public void run() {
				Game.this.setMapPoll(new MapPoll(game));
			}
		}, 200L);
	}

	public void endRound() {
		this.endRoundExtra();
		this.setRoundEnded(true);
		PlayerManager.clearHotBars();
		this.awardLastKiller();
		for (BPPlayer bpPlayer : this.players) {
			Breakpoint.getInstance().getNametagAPIHook().updateNametag(bpPlayer);
		}
	}

	public void awardLastKiller() {
		if (this.getLastBloodPlayerName() == null) {
			return;
		}
		Player player = Bukkit.getPlayerExact((String) this.getLastBloodPlayerName());
		if (player == null) {
			return;
		}
		BPPlayer bpPlayer = BPPlayer.get(player);
		if (Bukkit.getOnlinePlayers().size() >= Bukkit.getOnlinePlayers().size()/ 2) {
			bpPlayer.checkAchievement(AchievementType.LAST_BLOOD);
		}
		Location playerLoc = player.getLocation();
		String playerPVPName = bpPlayer.getPVPName();
		this.broadcast((Object) ChatColor.DARK_RED + "" + (Object) ChatColor.BOLD + "> LAST BLOOD! " + playerPVPName
				+ (Object) ChatColor.DARK_RED + (Object) ChatColor.BOLD + " <", false);
		PlayerManager.spawnRandomlyColoredFirework(playerLoc);
	}

	public void broadcastDeathMessage(String victim, String killer) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (!bpPlayer.isInGame() || !bpPlayer.getSettings().hasDeathMessages())
				continue;
			player.sendMessage(MessageType.PVP_KILLINFO_KILLED.getTranslation().getValue(victim, killer));
		}
	}

	public void broadcastDeathMessage(String victim) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (!bpPlayer.isInGame() || !bpPlayer.getSettings().hasDeathMessages())
				continue;
			player.sendMessage(MessageType.PVP_KILLINFO_DIED.getTranslation().getValue(victim));
		}
	}

	public void startCountdown() {
		if (this.getCountdownTaskId() != null) {
			Bukkit.getScheduler().cancelTask(this.getCountdownTaskId().intValue());
		}
		this.setMapSecondsLeft(540);
		this.scheduleNextSecond();
	}

	public void scheduleNextSecond() {
		Breakpoint plugin = Breakpoint.getInstance();
		this.setCountdownTaskId(
				plugin.getServer().getScheduler().scheduleSyncDelayedTask((Plugin) plugin, new Runnable() {

					@Override
					public void run() {
						Game.this.second();
					}
				}, 20L));
	}

	public int getRandomMapWithCapacity(int players) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (int i = 0; i < this.getMaps().size(); ++i) {
			BPMap map = this.getMaps().get(i);
			if (!map.isPlayable() || !map.isPlayableWith(players))
				continue;
			ids.add(i);
		}
		if (ids.size() > 0) {
			return (Integer) ids.get(new Random().nextInt(ids.size()));
		}
		return -1;
	}

	public void changeMap(int mapId) {
		this.setActiveMapId(mapId);
		BPMap map = this.getCurrentMap();
		String mapName = map.getName();
		map.setLastTimePlayed(System.currentTimeMillis());
		this.spawnPlayers();
		this.updateCurrentMapRenderer();
		this.setFirstBloodPlayerName(null);
		this.setLastBloodPlayerName(null);
		this.noPlayers = false;
		for (World world : Bukkit.getWorlds()) {
			world.setGameRuleValue("doDaylightCycle", "false");
		}
		for (BPPlayer bpPlayer : this.players) {
			Breakpoint.getInstance().getNametagAPIHook().updateNametag(bpPlayer);
		}
		this.setRoundEnded(false);
		this.startCountdown();
		this.changeMapExtra();
		this.broadcast(MessageType.MAP_CHANGE.getTranslation().getValue(mapName), true);
	}

	public void updateCurrentMapRenderer() {
		BPMap map = this.getCurrentMap();
		this.currentMapRenderer.setCurrentMap(map);
		if (Bukkit.getMap((short) this.currentMapMapId) != null) {
			MapManager.updateMapForNotPlayingPlayers(this.currentMapMapId);
		}
	}

	public void spawnPlayers() {
		for (BPPlayer bpPlayer : this.players) {
			bpPlayer.spawn();
		}
	}

	public void broadcastCombo(String playerName, Location loc, ChatColor color, String decor, String name) {
		this.broadcast((Object) color + "" + decor + " " + (Object) ChatColor.BOLD + name + "! - " + playerName
				+ (Object) color + " " + decor, false);
		PlayerManager.spawnRandomlyColoredFirework(loc);
	}

	public void broadcast(String string, boolean prefix) {
		for (BPPlayer bpPlayer : this.players) {
			if (!bpPlayer.isPlaying())
				continue;
			Player player = bpPlayer.getPlayer();
			player.sendMessage((prefix
					? new StringBuilder().append((Object) ChatColor.DARK_GRAY).append("[")
							.append((Object) ChatColor.LIGHT_PURPLE).append((Object) ChatColor.BOLD)
							.append("Breakpoint").append((Object) ChatColor.DARK_GRAY).append("] ").toString()
					: "") + (Object) ChatColor.YELLOW + string);
		}
		Bukkit.getConsoleSender().sendMessage("[Breakpoint] [" + this.name + "] " + string);
	}

	public void broadcast(String string) {
		this.broadcast(string, false);
	}

	public void broadcastTitle(String title, Team t) {
		for (BPPlayer bpPlayer : this.players) {
			if (!bpPlayer.isPlaying() || ((CTFProperties) bpPlayer.getGameProperties()).getTeam() != t)
				continue;
			((CraftPlayer) bpPlayer.getPlayer()).sendTitle(title, "");
		}
	}

	public void broadcastTitle(String title, String subtitle, Team t) {
		for (BPPlayer bpPlayer : this.players) {
			if (!bpPlayer.isPlaying() || ((CTFProperties) bpPlayer.getGameProperties()).getTeam() != t)
				continue;
			((CraftPlayer) bpPlayer.getPlayer()).sendTitle(title, subtitle);
		}
	}

	public BPMap getMapByName(String mapName) {
		for (BPMap map : this.getMaps()) {
			if (!mapName.equals(map.getName()))
				continue;
			return map;
		}
		return null;
	}

	public boolean votingInProgress() {
		if (this.getMapPoll() != null) {
			return this.getMapPoll().getVoting();
		}
		return false;
	}

	public boolean isMapActive(int index) {
		BPMap map = this.maps.get(index);
		return this.isMapActive(map);
	}

	public boolean isMapActive(BPMap map) {
		return this.getCurrentMap().equals(map);
	}

	public BPMap getCurrentMap() {
		int id = this.getActiveMapId();
		if (id == -1) {
			id = 0;
		}
		return this.getMaps().get(id);
	}

	public GameType getType() {
		return this.type;
	}

	public LinkedList<? extends BPMap> getPlayableMaps() {
		LinkedList<BPMap> playableMaps = new LinkedList<BPMap>();
		for (BPMap map : this.maps) {
			if (!map.isPlayable())
				continue;
			playableMaps.add(map);
		}
		return playableMaps;
	}

	public LinkedList<? extends BPMap> getMaps() {
		return this.maps;
	}

	public void setMaps(LinkedList<BPMap> maps) {
		this.maps = maps;
	}

	public List<BPPlayer> getPlayers() {
		return this.players;
	}

	public int getActiveMapId() {
		return this.activeMapId;
	}

	public void setActiveMapId(int activeMapId) {
		this.activeMapId = activeMapId;
	}

	public int getMapSecondsLeft() {
		return this.mapSecondsLeft;
	}

	public void setMapSecondsLeft(int mapSecondsLeft) {
		this.mapSecondsLeft = mapSecondsLeft;
	}

	public Integer getCountdownTaskId() {
		return this.countdownTaskId;
	}

	public void setCountdownTaskId(Integer countdownTaskId) {
		this.countdownTaskId = countdownTaskId;
	}

	public boolean hasRoundEnded() {
		return this.roundEnded;
	}

	public void setRoundEnded(boolean roundEnded) {
		this.roundEnded = roundEnded;
	}

	public MapPoll getMapPoll() {
		return this.mapPoll;
	}

	public void setMapPoll(MapPoll mapPoll) {
		this.mapPoll = mapPoll;
	}

	public String getFirstBloodPlayerName() {
		return this.firstBloodPlayerName;
	}

	public void setFirstBloodPlayerName(String firstBloodPlayerName) {
		this.firstBloodPlayerName = firstBloodPlayerName;
	}

	public String getLastBloodPlayerName() {
		return this.lastBloodPlayerName;
	}

	public void setLastBloodPlayerName(String lastBloodPlayerName) {
		this.lastBloodPlayerName = lastBloodPlayerName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getSignLocation() {
		return this.signLoc;
	}

	public void setSignLocation(Location signLoc) {
		this.signLoc = signLoc;
	}

	public boolean isActive() {
		return this.active;
	}

	private void setActive(boolean active) {
		this.active = active;
	}

	public short getVotingMapId() {
		return this.votingMapId;
	}

	public GameListener getListener() {
		return this.listener;
	}

}
