/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang.Validate
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapFont
 *  org.bukkit.map.MapView
 *  org.bukkit.map.MinecraftFont
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapFont;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;

public abstract class StatisticRenderer extends BPMapRenderer {
	public static final byte[] BACKGROUND_COLORS = new byte[] { BPMapPalette.getColor(13, 0),
			BPMapPalette.getColor(13, 1), BPMapPalette.getColor(13, 2), BPMapPalette.getColor(13, 1),
			BPMapPalette.getColor(13, 0) };
	public static final byte LABEL_COLOR = BPMapPalette.getColor(8, 2);
	public static final int FONT_SCALE_Y = 3;
	public static final int FONT_DISTANCE_FROM_CENTER = 2;
	public static final int BORDER = 2;
	private String label;
	private byte color;

	public StatisticRenderer(String label, byte color) {
		Validate.notNull((Object) label, (String) "The label cannot be null!");
		this.label = label;
		this.color = color;
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		String value = this.getValue();
		StatisticRenderer.drawRectangleFade(canvas, 0, 0, 128, 128, BACKGROUND_COLORS, 0);
		this.drawCenteredText(canvas, this.label, LABEL_COLOR, (-MinecraftFont.Font.getHeight()) * 3 - 2);
		if (value != null) {
			this.drawCenteredText(canvas, value, this.color, 2);
		}
		this.drawBorder(canvas, this.color, 2);
	}

	private void drawCenteredText(MapCanvas canvas, String text, byte color, int distanceFromCenter) {
		int labelWidth = StatisticRenderer.getWidth(text);
		int labelScaleX = this.getScaleX(text, labelWidth);
		int labelX = (128 - (labelWidth *= labelScaleX)) / 2;
		int labelY = 64 + distanceFromCenter;
		this.drawText(canvas, labelX, labelY, labelScaleX, 3, (MapFont) MinecraftFont.Font, text, color);
	}

	private void drawBorder(MapCanvas canvas, byte color, int width) {
		StatisticRenderer.drawRectangle(canvas, 0, 0, 2, 128, color);
		StatisticRenderer.drawRectangle(canvas, 126, 0, 2, 128, color);
		StatisticRenderer.drawRectangle(canvas, 2, 0, 124, 2, color);
		StatisticRenderer.drawRectangle(canvas, 2, 126, 124, 2, color);
	}

	public abstract String getValue();

	public int getScaleX(String string, int width) {
		return 126 / width;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		Validate.notNull((Object) label, (String) "The label cannot be null!");
		this.label = label;
	}

	public byte getColor() {
		return this.color;
	}

	public void setColor(byte color) {
		this.color = color;
	}
}
