/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapView
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.game.BPMap;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFMap;
import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import java.awt.image.BufferedImage;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapView;

public class CurrentMapRenderer extends BPMapRenderer {
	private byte[][] image;

	public CurrentMapRenderer(CTFMap map) {
		this.setCurrentMap(map);
	}

	public CurrentMapRenderer() {
		this(null);
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		if (this.image != null) {
			this.drawImage(canvas);
		}
	}

	public void setCurrentMap(BPMap map) {
		this.image = CurrentMapRenderer.toBytes(map != null ? map.getImage() : null);
	}

	public void drawImage(MapCanvas canvas) {
		for (int x = 0; x < this.image.length; ++x) {
			for (int y = 0; y < this.image[0].length; ++y) {
				canvas.setPixel(x, y, this.image[x][y]);
			}
		}
	}
}
