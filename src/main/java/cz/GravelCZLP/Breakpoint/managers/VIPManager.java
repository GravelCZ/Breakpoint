/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class VIPManager {
	public static void startLoops() {
		VIPManager.startReminderLoop();
		VIPManager.startLobbyFlyLoop();
	}

	public static void startReminderLoop() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) Breakpoint.getInstance(), new Runnable() {

			@Override
			public void run() {
				VIPManager.remindPlayers();
			}
		}, 1200L, 6000L);
	}

	public static void startLobbyFlyLoop() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) Breakpoint.getInstance(), new Runnable() {

			@Override
			public void run() {
				VIPManager.checkFlyingPlayersInLobby();
			}
		}, 100L, 100L);
	}

	public static void checkFlyingPlayersInLobby() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!player.isFlying() || player.hasPermission("Breakpoint.flyFromSpawn")
					|| !VIPManager.isFarFromSpawnToUseFly(player))
				continue;
			player.sendMessage(MessageType.COMMAND_FLY_TOOFAR.getTranslation().getValue(new Object[0]));
			player.setAllowFlight(false);
			player.setFlying(false);
		}
	}

	public static boolean isFarFromSpawnToUseFly(Player player) {
		Location lobbyLoc;
		Configuration config = Breakpoint.getBreakpointConfig();
		Location loc = player.getLocation();
		double distance = loc.distance(lobbyLoc = config.getLobbyLocation());
		return distance > 96.0;
	}

	public static void remindPlayers() {
		String feature = VIPManager.getRandomFeature();
		if (feature == null) {
			return;
		}
		feature = ChatColor.translateAlternateColorCodes((char) '&', (String) feature);
		for (Player player : Bukkit.getOnlinePlayers()) {
			boolean b = player.hasPermission("Breakpoint.admin") || player.hasPermission("Breakpoint.helper")
					|| player.hasPermission("Breakpoint.moderator") || player.hasPermission("Breakpoint.sponsor")
					|| player.hasPermission("Breakpoint.vipplus") || player.hasPermission("Breakpoint.vip");
			if (b)
				continue;
			VIPManager.remindPlayer(player, feature);
		}
	}

	public static void remindPlayer(Player player, String feature) {
		player.sendMessage((Object) ChatColor.DARK_GRAY + "#############################");
		player.sendMessage(MessageType.OTHER_VIPFEATURE.getTranslation().getValue(feature));
		player.sendMessage((Object) ChatColor.DARK_GRAY + "#############################");
	}

	public static String getRandomFeature() {
		Configuration config = Breakpoint.getBreakpointConfig();
		String[] features = config.getVIPFeatures();
		if (features == null) {
			return null;
		}
		int rnd = new Random().nextInt(features.length);
		return features[rnd];
	}

}
