/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapFont
 *  org.bukkit.map.MapView
 *  org.bukkit.map.MinecraftFont
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapFont;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;

public class SizeRenderer extends BPMapRenderer {
	private static final int fontHeight = 128 / MinecraftFont.Font.getHeight();
	byte foregroundColor;
	byte backgroundColor;
	int size;

	public SizeRenderer(byte foregroundColor, byte backgroundColor, int size) {
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.size = size;
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		SizeRenderer.drawRectangle(canvas, 0, 0, 128, 128, BPMapPalette.getColor(0, 0));
		this.drawSize(canvas);
	}

	public void drawSize(MapCanvas canvas) {
		String sSize = Integer.toString(this.size);
		int totalWidth = SizeRenderer.getWidth(sSize);
		int fontWidth = 128 / totalWidth;
		int startX = (128 - totalWidth * fontWidth) / 2;
		int startY = (128 - MinecraftFont.Font.getHeight() * fontHeight) / 2;
		SizeRenderer.drawRectangle(canvas, 0, 0, 128, 128, this.backgroundColor);
		this.drawText(canvas, startX, startY, fontWidth, fontHeight, (MapFont) MinecraftFont.Font, sSize,
				this.foregroundColor);
	}

	public void setSize(int size) {
		this.size = size;
	}
}
