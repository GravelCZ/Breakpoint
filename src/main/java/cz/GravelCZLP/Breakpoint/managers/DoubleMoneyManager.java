/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.World
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Item
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitTask
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFMap;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class DoubleMoneyManager {
	private static boolean doubleXP;

	public static void update() {
		DoubleMoneyManager.updateDoubleXP();
	}

	private static void updateDoubleXP() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(7);
		doubleXP = day == 1;
	}

	public static boolean isDoubleXP() {
		return doubleXP;
	}

	public static void startBoostLoop() {
		if (!DoubleMoneyManager.isDoubleXP()) {
			return;
		}
		Bukkit.getScheduler().runTaskTimer((Plugin) Breakpoint.getInstance(), new Runnable() {

			@Override
			public void run() {
				CTFMap map = null;
				for (Game g : GameManager.getGames()) {
					if (g.getType() != GameType.CTF)
						continue;
					CTFGame ctfGame = (CTFGame) g;
					map = ctfGame.getCurrentMap();
				}
				for (Location loc : map.getMelounBoostsLocations()) {
					for (Entity ent : loc.getWorld().getNearbyEntities(loc, 2.0, 2.0, 2.0)) {
						if (!(ent instanceof Item))
							continue;
						ent.remove();
					}
					ItemStack boost = new ItemStack(Material.SPECKLED_MELON);
					ItemMeta bm = boost.getItemMeta();
					bm.setDisplayName("melounBoost");
					boost.setItemMeta(bm);
					Location loc0 = loc.clone();
					loc.getWorld().dropItem(loc0, boost);
				}
			}
		}, 0L, Breakpoint.getBreakpointConfig().getTimeForSpeedMeloun());
	}

}
