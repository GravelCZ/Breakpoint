/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class AfkManager {
	public static int defSTK = 180;
	public static final String afkKickProtectionNode = "Breakpoint.afkProtection";
	Breakpoint plugin;
	int loopId;

	public AfkManager(Breakpoint p) {
		this.plugin = p;
	}

	public boolean executeAfk(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		if (player.hasPermission(afkKickProtectionNode)) {
			return false;
		}
		Location pastLocation = bpPlayer.getAfkPastLocation();
		Location presentLocation = player.getLocation();
		if (pastLocation != null && pastLocation.equals((Object) presentLocation)) {
			int secondsToKick = bpPlayer.getAfkSecondsToKick();
			if (secondsToKick <= 0) {
				player.kickPlayer(MessageType.OTHER_AFKKICK.getTranslation().getValue(new Object[0]));
				return true;
			}
			bpPlayer.setAfkSecondsToKick(secondsToKick - 1);
			bpPlayer.setAfkPastLocation(presentLocation);
			return false;
		}
		bpPlayer.clearAfkSecondsToKick();
		return false;
	}

	public void startLoop() {
		this.loopId = Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) this.plugin, new Runnable() {

			@Override
			public void run() {
				AfkManager.this.tick();
			}
		}, 0L, 20L);
	}

	public void tick() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (bpPlayer == null)
				continue;
			this.executeAfk(bpPlayer);
		}
	}

}
