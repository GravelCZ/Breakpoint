/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.language;

import cz.GravelCZLP.Breakpoint.language.Translation;

public interface Translateable {
	public String getDefaultTranslation();

	public String getYamlPath();

	public void setTranslation(Translation var1);

	public Translation getTranslation();
}
