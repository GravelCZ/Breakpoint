/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Color
 *  org.bukkit.Material
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.LeatherArmorMeta
 */
package cz.GravelCZLP.Breakpoint.equipment;

import cz.GravelCZLP.Breakpoint.equipment.BPEquipment;
import java.util.List;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class BPArmor extends BPEquipment {
	private int typeId;
	private int rgb;

	public BPArmor(int typeId, int rgb, String name, int minutesLeft) {
		super(name, minutesLeft);
		this.setTypeId(typeId);
		this.setRGB(rgb);
	}

	@Override
	public BPArmor clone() {
		return new BPArmor(this.typeId, this.rgb, this.getName(), this.getMinutesLeft());
	}

	@Override
	public String getEquipmentLabel() {
		return "armor";
	}

	@Override
	protected ItemStack getItemStackRaw() {
		Material mat = BPArmor.getMaterial(this.typeId);
		ItemStack is = new ItemStack(mat);
		LeatherArmorMeta im = (LeatherArmorMeta) is.getItemMeta();
		im.setDisplayName(this.getName());
		is.setItemMeta((ItemMeta) im);
		BPArmor.colorArmor(is, this.rgb);
		return is;
	}

	public void colorArmor(ItemStack armor) {
		BPArmor.colorArmor(armor, this.rgb);
	}

	public static void colorArmor(ItemStack armor, int rgb) {
		Color color = Color.fromRGB((int) rgb);
		BPArmor.colorArmor(armor, color);
	}

	public static void colorArmor(ItemStack armor, Color color) {
		LeatherArmorMeta im = (LeatherArmorMeta) armor.getItemMeta();
		im.setColor(color);
		armor.setItemMeta((ItemMeta) im);
	}

	@Override
	protected String serializeRaw() {
		return "" + this.typeId + "," + this.rgb + "," + this.getName() + "," + this.getMinutesLeft();
	}

	public static Material getMaterial(int typeId) {
		switch (typeId) {
		case 3: {
			return Material.LEATHER_HELMET;
		}
		case 2: {
			return Material.LEATHER_CHESTPLATE;
		}
		case 1: {
			return Material.LEATHER_LEGGINGS;
		}
		case 0: {
			return Material.LEATHER_BOOTS;
		}
		}
		return null;
	}

	public static int getTypeId(Material mat) {
		switch (mat) {
		case LEATHER_HELMET: {
			return 3;
		}
		case LEATHER_CHESTPLATE: {
			return 2;
		}
		case LEATHER_LEGGINGS: {
			return 1;
		}
		case LEATHER_BOOTS: {
			return 0;
		}
		}
		return -1;
	}

	public static boolean isConvertable(ItemStack is) {
		ItemMeta im;
		if (is.hasItemMeta() && (im = is.getItemMeta()) instanceof LeatherArmorMeta) {
			try {
				Integer.parseInt(((String) im.getLore().get(0)).substring(15));
				return true;
			} catch (Exception exception) {
				// empty catch block
			}
		}
		return false;
	}

	public int getTypeId() {
		return this.typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getRGB() {
		return this.rgb;
	}

	public void setRGB(int rgb) {
		this.rgb = rgb;
	}

}
