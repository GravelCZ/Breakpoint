/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.equipment.BPEquipment;
import cz.GravelCZLP.Breakpoint.equipment.BPSkull;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class SkullCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		Player player = (Player) sender;
		BPPlayer bpPlayer = BPPlayer.get(player);
		if (args.length <= 0) {
			player.sendMessage(SkullCommandExecutor.getCommandInfo(
					MessageType.COMMAND_SKULL_PATH.getTranslation().getValue(new Object[0]),
					MessageType.COMMAND_SKULL_DESC.getTranslation().getValue(new Object[0]), ChatColor.AQUA));
			player.sendMessage(MessageType.COMMAND_SKULL_WARNING.getTranslation().getValue(new Object[0]));
		} else {
			if (!bpPlayer.isInLobby()) {
				player.sendMessage(MessageType.COMMAND_SKULL_EXE_NOTINLOBBY.getTranslation().getValue(new Object[0]));
				return true;
			}
			PlayerInventory inv = player.getInventory();
			ItemStack helmet = inv.getHelmet();
			if (helmet == null) {
				player.sendMessage(
						MessageType.COMMAND_SKULL_EXE_NOTRENAMEABLE.getTranslation().getValue(new Object[0]));
				return true;
			}
			Material helmetMat = helmet.getType();
			if (helmetMat == Material.SKULL_ITEM) {
				BPSkull bpSkull = (BPSkull) BPEquipment.parse(helmet);
				if (bpSkull == null) {
					player.sendMessage(MessageType.OTHER_ERROR.getTranslation().getValue(new Object[0]));
					return true;
				}
				if (bpSkull.canBeRenamed()) {
					if (BPSkull.canBeRenamedTo(args[0])) {
						bpSkull.setName(args[0]);
						inv.setHelmet(bpSkull.getItemStack());
						player.sendMessage(MessageType.COMMAND_SKULL_EXE_SUCCESS.getTranslation().getValue(args[0]));
					} else {
						player.sendMessage(MessageType.COMMAND_SKULL_EXE_NAMEBANNED.getTranslation().getValue(args[0]));
					}
				} else {
					player.sendMessage(
							MessageType.COMMAND_SKULL_EXE_NOTRENAMEABLE.getTranslation().getValue(new Object[0]));
				}
			} else {
				player.sendMessage(
						MessageType.COMMAND_SKULL_EXE_NOTRENAMEABLE.getTranslation().getValue(new Object[0]));
			}
		}
		return true;
	}

	private static String getCommandInfo(String command, String info, ChatColor color) {
		return (Object) color + "/" + command + (Object) ChatColor.GRAY + " - " + info;
	}
}
