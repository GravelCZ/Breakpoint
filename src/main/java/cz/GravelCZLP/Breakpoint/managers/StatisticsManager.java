/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.command.CommandSender
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.achievements.Achievement;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.maps.MapManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.clans.Clan;
import cz.GravelCZLP.Breakpoint.statistics.PlayerStatistics;
import cz.GravelCZLP.Breakpoint.statistics.Statistics;
import cz.GravelCZLP.Breakpoint.statistics.TotalPlayerStatistics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import me.limeth.storageAPI.StorageType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public class StatisticsManager {
	public static final int MAX_AMOUNT = Integer.MAX_VALUE;
	private static TotalPlayerStatistics totalStats;
	public static List<PlayerStatistics> playersRankedByKills;
	private static List<Clan> clansRankedByPoints;
	private static boolean updating;

	public static void startLoop() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) Breakpoint.getInstance(), new Runnable() {

			@Override
			public void run() {
				StatisticsManager.asyncUpdate();
			}
		}, 0L, 12000L);
	}

	public static void asyncUpdate() {
		if (StatisticsManager.isUpdating()) {
			return;
		}
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				StatisticsManager.update();
			}
		}, "Async Rank Update");
		t.setPriority(5);
		t.start();
	}

	private static synchronized void update() {
		updating = true;
		StatisticsManager.updatePlayerRanksByKills();
		StatisticsManager.updateClanRanksByPoints();
		StatisticsManager.updateStatistics();
		StatisticsManager.updateMaps();
		updating = false;
		Breakpoint.warn("Statistics have been updated!");
	}

	public static void updateMaps() {
		Breakpoint.getInstance().getMapManager().update();
	}

	public static void updateStatistics() {
		TotalPlayerStatistics stats = new TotalPlayerStatistics();
		if (playersRankedByKills == null) {
			return;
		}
		for (PlayerStatistics stat : playersRankedByKills) {
			stats.add(stat);
		}
		totalStats = stats;
	}

	public static void updateClanRanksByPoints() {
		ArrayList<Clan> unorderedClans = new ArrayList<Clan>(Clan.clans);
		ArrayList<Clan> orderedClans = new ArrayList<Clan>();
		for (Clan clan : unorderedClans) {
			int orderedPoints;
			Clan ordered;
			int i;
			int points = clan.getPoints();
			for (i = 0; i < orderedClans.size()
					&& points <= (orderedPoints = (ordered = orderedClans.get(i)).getPoints())
					&& points != orderedPoints; ++i) {
			}
			orderedClans.add(i, clan);
		}
		clansRankedByPoints = orderedClans;
	}

	public static void updatePlayerRanksByKills() {
		Configuration config = Breakpoint.getBreakpointConfig();
		StorageType storageType = config.getStorageType();
		List<String> playerNames = BPPlayer.getPlayerNames(storageType);
		LinkedList<PlayerStatistics> unorderedList = new LinkedList<PlayerStatistics>();
		LinkedList<PlayerStatistics> orderedList = new LinkedList<PlayerStatistics>();
		if (playerNames == null) {
			return;
		}
		for (String playerName : playerNames) {
			if (!StatisticsManager.isPlayer(playerName))
				continue;
			PlayerStatistics stat = PlayerStatistics.loadPlayerStatistics(playerName);
			unorderedList.add(stat);
		}
		while (unorderedList.size() > 0 && orderedList.size() < Integer.MAX_VALUE) {
			PlayerStatistics topStat = null;
			for (PlayerStatistics stat : unorderedList) {
				int kills = stat.getKills();
				if (topStat != null && kills <= topStat.getKills())
					continue;
				topStat = stat;
			}
			if (topStat == null)
				break;
			unorderedList.remove(topStat);
			orderedList.add(topStat);
		}
		playersRankedByKills = orderedList;
	}

	public static Integer getRank(String playerName) {
		if (playersRankedByKills == null) {
			return null;
		}
		for (int i = 0; i < playersRankedByKills.size(); ++i) {
			PlayerStatistics stat = playersRankedByKills.get(i);
			if (!stat.getName().equals(playerName))
				continue;
			return i + 1;
		}
		return 0;
	}

	public static Integer getRank(Clan clan) {
		if (clansRankedByPoints == null) {
			return null;
		}
		for (int i = 0; i < clansRankedByPoints.size(); ++i) {
			Clan curClan = clansRankedByPoints.get(i);
			if (!curClan.equals(clan))
				continue;
			return i + 1;
		}
		return 0;
	}

	public static PlayerStatistics getPlayerStatistics(String playerName) {
		if (playersRankedByKills == null) {
			return null;
		}
		for (PlayerStatistics stat : playersRankedByKills) {
			if (!stat.getName().equals(playerName))
				continue;
			return stat;
		}
		return null;
	}

	private static boolean isPlayer(String playerName) {
		for (OfflinePlayer op : Bukkit.getOperators()) {
			if (!op.getName().equalsIgnoreCase(playerName))
				continue;
			return false;
		}
		return true;
	}

	public static void listTopClans(CommandSender sender, int length, int page) {
		if (clansRankedByPoints == null) {
			sender.sendMessage(MessageType.RANK_TOP_UPDATING.getTranslation().getValue(new Object[0]));
		}
		if (length * (page - 1) + 1 > clansRankedByPoints.size()) {
			sender.sendMessage(MessageType.RANK_TOP_CLANEMPTYPAGE.getTranslation().getValue(new Object[0]));
			return;
		}
		for (int i = length * (page - 1); i < length * page && i < clansRankedByPoints.size(); ++i) {
			Clan clan = clansRankedByPoints.get(i);
			String clanName = clan.getColoredName();
			int points = clan.getPoints();
			sender.sendMessage(MessageType.RANK_TOP_CLANFORMAT.getTranslation().getValue(i + 1, clanName, points));
		}
	}

	public static void listTopPlayers(CommandSender sender, int length, int page) {
		if (playersRankedByKills == null) {
			sender.sendMessage(MessageType.RANK_TOP_UPDATING.getTranslation().getValue(new Object[0]));
		}
		if (length * (page - 1) + 1 > playersRankedByKills.size()) {
			sender.sendMessage(MessageType.RANK_TOP_EMPTYPAGE.getTranslation().getValue(new Object[0]));
			return;
		}
		for (int i = length * (page - 1); i < length * page && i < playersRankedByKills.size(); ++i) {
			Statistics stat = playersRankedByKills.get(i);
			String playerName = stat.getName();
			int kills = stat.getKills();
			int deaths = stat.getDeaths();
			String kdr = InventoryMenuManager.trimKdr(Double.toString((double) kills / (double) deaths));
			sender.sendMessage(
					MessageType.RANK_TOP_FORMAT.getTranslation().getValue(i + 1, playerName, kills, deaths, kdr));
		}
	}

	public static String format(String label, Object amount) {
		return (Object) ChatColor.GRAY + label + ": " + (Object) ChatColor.YELLOW + amount;
	}

	public static void showStatistics(CommandSender sender, String targetName) {
		PlayerStatistics stats = StatisticsManager.getPlayerStatistics(targetName);
		if (stats == null) {
			sender.sendMessage(MessageType.RANK_PLAYER_NOTFOUND.getTranslation().getValue(targetName));
			return;
		}
		Integer rawRank = StatisticsManager.getRank(targetName);
		String rank = rawRank != null ? Integer.toString(rawRank) : "?";
		int kills = stats.getKills();
		int deaths = stats.getDeaths();
		String kdr = Double.toString((double) kills / (double) deaths);
		kdr = InventoryMenuManager.trimKdr(kdr);
		int achievements = Achievement.getUnlockedAchievementAmount(targetName);
		int bought = stats.getBought();
		int flagTakes = stats.getFlagTakes();
		int flagCaptures = stats.getFlagCaptures();
		sender.sendMessage(MessageType.RANK_PLAYER_DESC.getTranslation().getValue(targetName, rank));
		sender.sendMessage(MessageType.RANK_PLAYER_KILLS.getTranslation().getValue(kills));
		sender.sendMessage(MessageType.RANK_PLAYER_DEATHS.getTranslation().getValue(deaths));
		sender.sendMessage(MessageType.RANK_PLAYER_KDR.getTranslation().getValue(kdr));
		sender.sendMessage(MessageType.RANK_PLAYER_ACHIEVEMENTS.getTranslation().getValue(achievements));
		sender.sendMessage(MessageType.RANK_PLAYER_MERCHANDISEPURCHASED.getTranslation().getValue(bought));
		sender.sendMessage(MessageType.RANK_PLAYER_CRYSTALSSTOLEN.getTranslation().getValue(flagTakes));
		sender.sendMessage(MessageType.RANK_PLAYER_CRYSTALSCAPTURED.getTranslation().getValue(flagCaptures));
	}

	public static boolean isUpdating() {
		return updating;
	}

	public static boolean hasTotalStats() {
		return totalStats != null;
	}

	public static TotalPlayerStatistics getTotalStats() {
		return totalStats;
	}

	static {
		updating = false;
	}

}
