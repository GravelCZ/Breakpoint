/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.sound;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public enum BPSound {
	MULTI_KILL("gravelczlp.breakpoint.combo.multikill"), MEGA_KILL("gravelczlp.breakpoint.combo.megakill"), ULTRA_KILL(
			"gravelczlp.breakpoint.combo.ultrakill"), MONSTER_KILL(
					"gravelczlp.breakpoint.combo.monsterkill"), LUDACRISS_KILL(
							"gravelczlp.breakpoint.combo.ludacrisskill"), KILLING_SPREE(
									"gravelczlp.breakpoint.combo.killingspree"), RAMPAGE(
											"gravelczlp.breakpoint.combo.rampage"), DOMINATING(
													"gravelczlp.breakpoint.combo.dominating"), UNSTOPPABLE(
															"gravelczlp.breakpoint.combo.unstoppable"), GODLIKE(
																	"gravelczlp.breakpoint.combo.godlike"), MASSACRE(
																			"gravelczlp.breakpoint.combo.massacre"), FIRST_BLOOD(
																					"gravelczlp.breakpoint.misc.firstblood"), HEADSHOT(
																							"gravelczlp.breakpoint.misc.headshot"), FLAWLESS_VICTORY(
																									"gravelczlp.breakpoint.misc.flawlessvictory"), HUMILIATING_DEFEAT(
																											"gravelczlp.breakpoint.misc.humiliatingdefeat"), MINUTES(
																													"gravelczlp.breakpoint.countdown.minutes",
																													0.8285714285714286), SECONDS(
																															"gravelczlp.breakpoint.countdown.seconds",
																															0.8732879818594105), REMAINING(
																																	"gravelczlp.breakpoint.countdown.remaining",
																																	1.0490702947845805), SIXTY(
																																			"gravelczlp.breakpoint.countdown.sixty",
																																			0.9913832199546485), FIFTY(
																																					"gravelczlp.breakpoint.countdown.fifty",
																																					0.9979138321995464), FOURTY(
																																							"gravelczlp.breakpoint.countdown.fourty",
																																							0.9027664399092971), THIRTY(
																																									"gravelczlp.breakpoint.countdown.thirty",
																																									0.8673015873015874), TWENTY(
																																											"gravelczlp.breakpoint.countdown.twenty",
																																											0.8224943310657596), TEN(
																																													"gravelczlp.breakpoint.countdown.ten",
																																													0.5822222222222222), NINE(
																																															"gravelczlp.breakpoint.countdown.nine",
																																															0.8317460317460318), EIGHT(
																																																	"gravelczlp.breakpoint.countdown.eight",
																																																	0.5796825396825397), SEVEN(
																																																			"gravelczlp.breakpoint.countdown.seven",
																																																			0.7435827664399093), SIX(
																																																					"gravelczlp.breakpoint.countdown.six",
																																																					0.8317460317460318), FIVE(
																																																							"gravelczlp.breakpoint.countdown.five",
																																																							0.8789115646258503), FOUR(
																																																									"gravelczlp.breakpoint.countdown.four",
																																																									0.7764172335600907), THREE(
																																																											"gravelczlp.breakpoint.countdown.three",
																																																											0.7763265306122449), TWO(
																																																													"gravelczlp.breakpoint.countdown.two",
																																																													0.6738321995464852), ONE(
																																																															"gravelczlp.breakpoint.countdown.one",
																																																															0.7031292517006803);

	private final String path;
	private final Double lengthInSeconds;

	private BPSound(String path) {
		this(path, null);
	}

	private BPSound(String path, Double lengthInSeconds) {
		this.path = path;
		this.lengthInSeconds = lengthInSeconds;
	}

	public void play(Player player, Location loc, float volume, float pitch) {
		player.playSound(loc, this.path, volume, pitch);
	}

	public void play(Player player, float volume, float pitch) {
		this.play(player, player.getLocation(), volume, pitch);
	}

	public void play(Player player, float volume) {
		this.play(player, volume, 1.0f);
	}

	public void play(Player player) {
		this.play(player, 1.0f);
	}

	public String getPath() {
		return this.path;
	}

	public boolean hasLength() {
		return this.lengthInSeconds != null;
	}

	public double getLengthInSeconds() {
		return this.hasLength() ? this.lengthInSeconds : 0.0;
	}

	public static BPSound parse(int number) {
		switch (number) {
		default: {
			return null;
		}
		case 1: {
			return ONE;
		}
		case 2: {
			return TWO;
		}
		case 3: {
			return THREE;
		}
		case 4: {
			return FOUR;
		}
		case 5: {
			return FIVE;
		}
		case 6: {
			return SIX;
		}
		case 7: {
			return SEVEN;
		}
		case 8: {
			return EIGHT;
		}
		case 9: {
			return NINE;
		}
		case 10: {
			return TEN;
		}
		case 20: {
			return TWENTY;
		}
		case 30: {
			return THIRTY;
		}
		case 40: {
			return FOURTY;
		}
		case 50: {
			return FIFTY;
		}
		case 60:
		}
		return SIXTY;
	}
}
