/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.StatisticsManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RankCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length <= 0) {
			sender.sendMessage(RankCommandExecutor.getCommandInfo(
					MessageType.COMMAND_RANK_PATH.getTranslation().getValue(new Object[0]),
					MessageType.COMMAND_RANK_DESC.getTranslation().getValue(new Object[0]), ChatColor.AQUA));
		} else {
			StatisticsManager.showStatistics(sender, args[0]);
		}
		return true;
	}

	private static String getCommandInfo(String command, String info, ChatColor color) {
		return (Object) color + "/" + command + (Object) ChatColor.GRAY + " - " + info;
	}
}
