/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.Player
 *  org.bukkit.event.inventory.InventoryClickEvent
 *  org.bukkit.inventory.Inventory
 *  org.bukkit.inventory.InventoryHolder
 *  org.bukkit.inventory.InventoryView
 *  org.bukkit.inventory.ItemStack
 */
package cz.GravelCZLP.Breakpoint.players;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.Arrays;
import java.util.List;
import me.limeth.storageAPI.Column;
import me.limeth.storageAPI.ColumnType;
import me.limeth.storageAPI.Storage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

public class Settings {
	private boolean deathMessages;
	private boolean extraSounds;
	private boolean showEnchantments;
	public static final String MENU_TITLE = "" + (Object) ChatColor.DARK_PURPLE + (Object) ChatColor.BOLD
			+ "BREAKPOINT " + (Object) ChatColor.RESET + "NASTAVENI";
	private static final SettingsButton[] SETTINGS_BUTTONS = new SettingsButton[] { new SettingsButton(
			MessageType.MENU_SETTINGS_DEATHMESSAGES_DESC, MessageType.MENU_SETTINGS_DEATHMESSAGES_TURNON,
			MessageType.MENU_SETTINGS_DEATHMESSAGES_TURNOFF, MessageType.MENU_SETTINGS_DEATHMESSAGES_ENABLE,
			MessageType.MENU_SETTINGS_DEATHMESSAGES_DISABLE, 3) {

		@Override
		public boolean isEnabled(Settings settings) {
			return settings.deathMessages;
		}

		@Override
		public void setEnabled(Settings settings, boolean enabled) {
			settings.deathMessages = enabled;
		}
	}, new SettingsButton(MessageType.MENU_SETTINGS_EXTRASOUNDS_DESC, MessageType.MENU_SETTINGS_EXTRASOUNDS_TURNON,
			MessageType.MENU_SETTINGS_EXTRASOUNDS_TURNOFF, MessageType.MENU_SETTINGS_EXTRASOUNDS_ENABLE,
			MessageType.MENU_SETTINGS_EXTRASOUNDS_DISABLE, 4) {

		@Override
		public boolean isEnabled(Settings settings) {
			return settings.extraSounds;
		}

		@Override
		public void setEnabled(Settings settings, boolean enabled) {
			settings.extraSounds = enabled;
		}
	}, new SettingsButton(MessageType.MENU_SETTINGS_SHOWENCHANTMENTS_DESC,
			MessageType.MENU_SETTINGS_SHOWENCHANTMENTS_TURNON, MessageType.MENU_SETTINGS_SHOWENCHANTMENTS_TURNOFF,
			MessageType.MENU_SETTINGS_SHOWENCHANTMENTS_ENABLE, MessageType.MENU_SETTINGS_SHOWENCHANTMENTS_DISABLE, 5) {

		@Override
		public boolean isEnabled(Settings settings) {
			return settings.showEnchantments;
		}

		@Override
		public void setEnabled(Settings settings, boolean enabled) {
			settings.showEnchantments = enabled;
		}
	} };

	public Settings() {
		this.deathMessages = true;
		this.extraSounds = true;
		this.showEnchantments = true;
	}

	public Settings(boolean deathMessages, boolean extraSounds, boolean showEnchantments) {
		this.deathMessages = deathMessages;
		this.extraSounds = extraSounds;
		this.showEnchantments = showEnchantments;
	}

	public static final Settings load(Storage storage) throws Exception {
		boolean deathMessages = storage.get(Boolean.class, "deathMessages", true);
		boolean extraSounds = storage.get(Boolean.class, "extraSounds", true);
		boolean showEnchantments = storage.get(Boolean.class, "showEnchantments", true);
		return new Settings(deathMessages, extraSounds, showEnchantments);
	}

	public void save(Storage storage) {
		storage.put("deathMessages", (Object) this.deathMessages);
		storage.put("extraSounds", (Object) this.extraSounds);
		storage.put("showEnchantments", (Object) this.showEnchantments);
	}

	public static List<Column> getRequiredMySQLColumns() {
		return Arrays.asList(new Column("deathMessages", ColumnType.BOOLEAN, new String[0]),
				new Column("extraSounds", ColumnType.BOOLEAN, new String[0]),
				new Column("showEnchantments", ColumnType.BOOLEAN, new String[0]));
	}

	public boolean areDefault() {
		return this.deathMessages && this.extraSounds && this.showEnchantments;
	}

	public boolean toggleExtraSounds() {
		this.extraSounds = !this.extraSounds;
		return this.extraSounds;
	}

	public boolean hasExtraSounds() {
		return this.extraSounds;
	}

	public void setExtraSounds(boolean extraSounds) {
		this.extraSounds = extraSounds;
	}

	public boolean toggleDeathMessages() {
		this.deathMessages = !this.deathMessages;
		return this.deathMessages;
	}

	public boolean hasDeathMessages() {
		return this.deathMessages;
	}

	public void setDeathMessages(boolean deathMessages) {
		this.deathMessages = deathMessages;
	}

	public boolean hasShowEnchantments() {
		return this.showEnchantments;
	}

	public void setShowEnchantments(boolean showEnchantments) {
		this.showEnchantments = showEnchantments;
	}

	public static InventoryView showSettingsMenu(BPPlayer bpPlayer) {
		Settings settings = bpPlayer.getSettings();
		Player player = bpPlayer.getPlayer();
		Inventory inv = Bukkit.getServer().createInventory((InventoryHolder) player, 9, MENU_TITLE);
		Settings.equipMenu(inv, settings);
		player.closeInventory();
		return player.openInventory(inv);
	}

	public static void equipMenu(Inventory inv, Settings settings) {
		for (SettingsButton button : SETTINGS_BUTTONS) {
			inv.setItem(button.slotId, button.getButton(settings));
		}
	}

	public static void onMenuClick(InventoryClickEvent event, BPPlayer bpPlayer) {
		event.setCancelled(true);
		int slotId = event.getRawSlot();
		for (SettingsButton button : SETTINGS_BUTTONS) {
			boolean newState;
			if (button.getSlotId() != slotId)
				continue;
			Player player = bpPlayer.getPlayer();
			Settings settings = bpPlayer.getSettings();
			Inventory inv = event.getInventory();
			boolean bl = newState = !button.isEnabled(settings);
			if (newState) {
				player.sendMessage(button.getEnableMessage().getTranslation().getValue(new Object[0]));
			} else {
				player.sendMessage(button.getDisableMessage().getTranslation().getValue(new Object[0]));
			}
			button.setEnabled(settings, newState);
			inv.setItem(slotId, button.getButton(newState));
			InventoryMenuManager.updateInventoryDelayed(player);
			break;
		}
	}

	public static abstract class SettingsButton {
		private MessageType desc;
		private MessageType turnOn;
		private MessageType turnOff;
		private MessageType enableMessage;
		private MessageType disableMessage;
		private int slotId;

		public SettingsButton(MessageType desc, MessageType turnOn, MessageType turnOff, MessageType enableMessage,
				MessageType disableMessage, int slotId) {
			this.desc = desc;
			this.turnOn = turnOn;
			this.turnOff = turnOff;
			this.enableMessage = enableMessage;
			this.disableMessage = disableMessage;
			this.setSlotId(slotId);
		}

		public abstract boolean isEnabled(Settings var1);

		public abstract void setEnabled(Settings var1, boolean var2);

		public ItemStack getButton(boolean enabled) {
			return InventoryMenuManager.getToggleableButton(this.turnOn, this.turnOff, this.desc, enabled);
		}

		public ItemStack getButton(Settings settings) {
			return this.getButton(this.isEnabled(settings));
		}

		public MessageType getDesc() {
			return this.desc;
		}

		public void setDesc(MessageType desc) {
			this.desc = desc;
		}

		public MessageType getTurnOn() {
			return this.turnOn;
		}

		public void setTurnOn(MessageType turnOn) {
			this.turnOn = turnOn;
		}

		public MessageType getTurnOff() {
			return this.turnOff;
		}

		public void setTurnOff(MessageType turnOff) {
			this.turnOff = turnOff;
		}

		public int getSlotId() {
			return this.slotId;
		}

		public void setSlotId(int slotId) {
			this.slotId = slotId;
		}

		public MessageType getEnableMessage() {
			return this.enableMessage;
		}

		public void setEnableMessage(MessageType enableMessage) {
			this.enableMessage = enableMessage;
		}

		public MessageType getDisableMessage() {
			return this.disableMessage;
		}

		public void setDisableMessage(MessageType disableMessage) {
			this.disableMessage = disableMessage;
		}
	}

}
