/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.CommandSender
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.CommandSender;

public class BreakpointCommand {
	List<BreakpointCommandExternalExcetuor> executors = new ArrayList<BreakpointCommandExternalExcetuor>();

	public void addExecutor(BreakpointCommandExternalExcetuor exe) {
		this.executors.add(exe);
	}

	public void callAllExecutors(String[] args, CommandSender sender) {
		for (BreakpointCommandExternalExcetuor exe : this.executors) {
			exe.onCommand(args, sender);
		}
	}

	public static interface BreakpointCommandExternalExcetuor {
		public void onCommand(String[] var1, CommandSender var2);
	}

}
