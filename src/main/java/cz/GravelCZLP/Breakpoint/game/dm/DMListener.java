/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Effect
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.block.BlockState
 *  org.bukkit.block.Sign
 *  org.bukkit.entity.LivingEntity
 *  org.bukkit.entity.Player
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityShootBowEvent
 *  org.bukkit.event.entity.PlayerDeathEvent
 *  org.bukkit.event.entity.PotionSplashEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerRespawnEvent
 *  org.bukkit.event.player.PlayerTeleportEvent
 *  org.bukkit.inventory.ItemStack
 */
package cz.GravelCZLP.Breakpoint.game.dm;

import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameListener;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.dm.DMGame;
import cz.GravelCZLP.Breakpoint.game.dm.DMProperties;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.perks.Perk;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

public class DMListener extends GameListener {
	public DMListener(Game game) {
		super(game, DMGame.class);
	}

	@Override
	public boolean onPlayerChat(AsyncPlayerChatEvent event, BPPlayer bpPlayer) {
		return true;
	}

	@Override
	public void onPlayerDeath(PlayerDeathEvent event, BPPlayer bpPlayer) {
		DMGame game = this.getGame();
		game.increasePointsOfOthers(bpPlayer);
		Player player = bpPlayer.getPlayer();
		Location loc = player.getLocation();
		Location effectLoc = loc.clone().add(0.0, 1.0, 0.0);
		Player killer = player.getKiller();
		if (killer != null) {
			BPPlayer bpKiller = BPPlayer.get(killer);
			game.increasePoints(bpKiller);
		}
		game.updateProgressObjectiveScores();
		DMListener.showBlood(effectLoc);
	}

	public static void showBlood(Location loc) {
		World world = loc.getWorld();
		world.playEffect(loc, Effect.STEP_SOUND, 152);
		world.playEffect(loc, Effect.STEP_SOUND, 55);
	}

	@Override
	public void onPlayerRespawn(PlayerRespawnEvent event, BPPlayer bpPlayer, boolean leaveAfterDeath) {
		DMProperties props = (DMProperties) bpPlayer.getGameProperties();
		CharacterType qct = bpPlayer.getQueueCharacter();
		if (qct != null) {
			if (qct != null) {
				props.chooseCharacter(qct, false);
			}
			bpPlayer.setQueueCharacter(null);
		}
		if (!leaveAfterDeath) {
			DMGame game = this.getGame();
			game.spawn(bpPlayer);
		}
	}

	@Override
	public void onEntityDamage(EntityDamageEvent dmgEvent) {
	}

	@Override
	public void onPlayerShootBow(EntityShootBowEvent event, BPPlayer bpPlayer) {
	}

	@Override
	public void onPlayerSplashedByPotion(PotionSplashEvent event, BPPlayer bpShooter, BPPlayer bpVictim) {
		DMGame game = this.getGame();
		if (game.hasRoundEnded()) {
			event.setIntensity((LivingEntity) bpVictim.getPlayer(), 0.0);
		}
		if (bpVictim.equals(bpShooter)) {
			event.setIntensity((LivingEntity) bpVictim.getPlayer(), 0.0);
		}
	}

	@Override
	public void onPlayerRightClickBlock(PlayerInteractEvent event, BPPlayer bpPlayer) {
		Sign sign;
		String[] lines;
		Block block = event.getClickedBlock();
		Material mat = block.getType();
		if ((mat == Material.WALL_SIGN || mat == Material.SIGN_POST)
				&& ChatColor.stripColor((String) (lines = (sign = (Sign) block.getState()).getLines())[0])
						.equals(MessageType.CHARACTER_SELECT.getTranslation().getValue(new Object[0]))) {
			Player player = bpPlayer.getPlayer();
			DMProperties props = (DMProperties) bpPlayer.getGameProperties();
			CharacterType selectedCT = props.getCharacterType();
			if (selectedCT == null) {
				String rawCharType = ChatColor.stripColor((String) lines[1]);
				CharacterType charType = null;
				for (CharacterType ct : CharacterType.values()) {
					if (!rawCharType.equalsIgnoreCase(ct.getProperName()))
						continue;
					charType = ct;
					break;
				}
				if (charType != null) {
					String name = charType.getProperName();
					if (charType.requiresVIP()
							&& !bpPlayer.getPlayer().hasPermission("Breakpoint.kit." + charType.name().toLowerCase())) {
						player.sendMessage((Object) ChatColor.DARK_GRAY + "---");
						player.sendMessage(MessageType.LOBBY_CHARACTER_VIPSONLY.getTranslation().getValue(name));
						player.sendMessage((Object) ChatColor.DARK_GRAY + "---");
						return;
					}
					props.chooseCharacter(charType, true);
					player.sendMessage(MessageType.LOBBY_CHARACTER_SELECTED.getTranslation().getValue(name));
				} else {
					player.sendMessage(MessageType.LOBBY_CHARACTER_NOTFOUND.getTranslation().getValue(rawCharType));
				}
			} else {
				String charName = selectedCT.getProperName();
				player.sendMessage(MessageType.LOBBY_CHARACTER_ALREADYSELECTED.getTranslation().getValue(charName));
			}
		}
	}

	@Override
	public void onPlayerPhysicallyInteractWithBlock(PlayerInteractEvent event, BPPlayer bpPlayer, Block blockBelow) {
	}

	@Override
	public void onPlayerRightClickItem(PlayerInteractEvent event, BPPlayer bpPlayer, ItemStack item) {
	}

	@Override
	public void onPlayerLeftClickItem(PlayerInteractEvent event, BPPlayer bpPlayer, ItemStack item) {
	}

	@Override
	public void onPlayerTeleport(PlayerTeleportEvent event, BPPlayer bpPlayer) {
	}

	@Override
	public DMGame getGame() {
		return (DMGame) super.getGame();
	}

	@Override
	public void onPlayerMove(BPPlayer bpPlayer, Location from, Location to, PlayerMoveEvent e) {
		Perk.onMove(bpPlayer, e);
	}
}
