/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.achievements.Achievement;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AchievementsCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		Player player = (Player) sender;
		BPPlayer bpPlayer = BPPlayer.get(player);
		if (args.length <= 0) {
			sender.sendMessage(AchievementsCommandExecutor.getCommandInfo(
					MessageType.COMMAND_ACHIEVEMENTS_PATH.getTranslation().getValue(new Object[0]),
					MessageType.COMMAND_ACHIEVEMENTS_DESC.getTranslation().getValue(new Object[0]), ChatColor.AQUA));
		} else {
			String targetName = args[0];
			Player target = Bukkit.getPlayer((String) targetName);
			BPPlayer bpTarget = BPPlayer.get(target);
			if (bpTarget != null) {
				int page = 0;
				if (args.length >= 2) {
					try {
						page = Integer.parseInt(args[1]);
					} catch (NumberFormatException numberFormatException) {
						// empty catch block
					}
				}
				bpPlayer.setAchievementViewTarget(bpTarget);
				bpPlayer.setAchievementViewPage(page);
				Achievement.showAchievementMenu(bpPlayer);
			} else {
				player.sendMessage(
						MessageType.COMMAND_ACHIEVEMENTS_EXE_PLAYEROFFLINE.getTranslation().getValue(targetName));
			}
		}
		return true;
	}

	private static String getCommandInfo(String command, String info, ChatColor color) {
		return (Object) color + "/" + command + (Object) ChatColor.GRAY + " - " + info;
	}
}
