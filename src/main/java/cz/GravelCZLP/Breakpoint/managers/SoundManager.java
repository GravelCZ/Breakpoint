/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.Location
 *  org.bukkit.Sound
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitScheduler
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.Settings;
import cz.GravelCZLP.Breakpoint.sound.BPSound;
import cz.GravelCZLP.Breakpoint.sound.BPSoundSet;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class SoundManager {
	public static final String BASE_PATH = "gravelczlp.breakpoint";
	public static final String COMBO_PATH = "gravelczlp.breakpoint.combo";
	public static final String MISC_PATH = "gravelczlp.breakpoint.misc";
	public static final String COUNTDOWN_PATH = "gravelczlp.breakpoint.countdown";

	public static void informPlayersAboutTime(Breakpoint plugin, Game game, int secondsRemaining) {
		if (secondsRemaining % 60 == 0 && secondsRemaining > 60) {
			int minutesRemaining = secondsRemaining / 60;
			if (minutesRemaining == 5 || minutesRemaining == 3 || minutesRemaining == 2) {
				BPSound sound = BPSound.parse(minutesRemaining);
				SoundManager.playSetForPlayers(plugin, new BPSoundSet(sound, BPSound.MINUTES, BPSound.REMAINING), game);
			}
		} else if (secondsRemaining <= 60 && secondsRemaining % 10 == 0 && secondsRemaining > 10) {
			BPSound sound = BPSound.parse(secondsRemaining);
			SoundManager.playSetForPlayers(plugin, new BPSoundSet(sound, BPSound.SECONDS, BPSound.REMAINING), game);
		} else if (secondsRemaining <= 10 && secondsRemaining > 0) {
			BPSound sound = BPSound.parse(secondsRemaining);
			SoundManager.playSoundForPlayers(game, sound);
		}
	}

	public static void playSetForPlayers(Breakpoint plugin, BPSoundSet set, final Game game) {
		BukkitScheduler scheduler = Bukkit.getScheduler();
		double delay = 0.0;
		for (final BPSound sound : set.getSounds()) {
			scheduler.scheduleSyncDelayedTask((Plugin) plugin, new Runnable() {

				@Override
				public void run() {
					SoundManager.playSoundForPlayers(game, sound);
				}
			}, (long) (delay * 20.0));
			delay += sound.getLengthInSeconds();
		}
	}

	public static void playSoundAt(BPSound sound, Location loc) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (!bpPlayer.isInGame())
				continue;
			SoundManager.tryPlay(player, bpPlayer, sound, loc);
		}
	}

	public static void playTeamSound(BPSound sound, CTFGame game, Team team, float volume, float pitch) {
		for (BPPlayer bpPlayer : game.getPlayers()) {
			Team pTeam = ((CTFProperties) bpPlayer.getGameProperties()).getTeam();
			if (pTeam != team)
				continue;
			Player player = bpPlayer.getPlayer();
			SoundManager.tryPlay(player, bpPlayer, sound, volume, pitch);
		}
	}

	public static void playTeamSound(BPSound sound, CTFGame game, Team team, float volume) {
		SoundManager.playTeamSound(sound, game, team, volume, 1.0f);
	}

	public static void playTeamSound(BPSound sound, CTFGame game, Team team) {
		SoundManager.playTeamSound(sound, game, team, 1.0f);
	}

	public static void playSoundForPlayers(Game game, BPSound sound) {
		for (BPPlayer bpPlayer : BPPlayer.onlinePlayers) {
			Game pGame = bpPlayer.getGame();
			if (!game.equals(pGame))
				continue;
			Player player = bpPlayer.getPlayer();
			SoundManager.tryPlay(player, bpPlayer, sound);
		}
	}

	public static void tryPlay(Player player, BPPlayer bpPlayer, BPSound sound, Location loc, float volume,
			float pitch) {
		if (!bpPlayer.getSettings().hasExtraSounds()) {
			return;
		}
		sound.play(player, loc, volume, pitch);
	}

	public static void tryPlay(Player player, BPPlayer bpPlayer, BPSound sound, float volume, float pitch) {
		SoundManager.tryPlay(player, bpPlayer, sound, player.getLocation(), volume, pitch);
	}

	public static void tryPlay(Player player, BPPlayer bpPlayer, BPSound sound, float volume) {
		SoundManager.tryPlay(player, bpPlayer, sound, volume, 1.0f);
	}

	public static void tryPlay(Player player, BPPlayer bpPlayer, BPSound sound) {
		SoundManager.tryPlay(player, bpPlayer, sound, 1.0f);
	}

	public static void tryPlay(Player player, BPPlayer bpPlayer, BPSound sound, Location loc, float volume) {
		SoundManager.tryPlay(player, bpPlayer, sound, loc, volume, 1.0f);
	}

	public static void tryPlay(Player player, BPPlayer bpPlayer, BPSound sound, Location loc) {
		SoundManager.tryPlay(player, bpPlayer, sound, loc, 1.0f);
	}

	public static void playTeamSound(CTFGame game, Location loc, Sound sound, float volume, float pitch, Team team) {
		for (BPPlayer bpPlayer : game.getPlayersInTeam(team)) {
			Player player = bpPlayer.getPlayer();
			player.playSound(loc, sound, volume, pitch);
		}
	}

	public static void playTeamSound(CTFGame game, Sound sound, float volume, float pitch, Team team) {
		for (BPPlayer bpPlayer : game.getPlayersInTeam(team)) {
			Player player = bpPlayer.getPlayer();
			Location loc = player.getLocation();
			player.playSound(loc, sound, volume, pitch);
		}
	}

}
