/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.equipment;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;

public enum ArmorMerchandiseType {
	BOOTS(MessageType.SHOP_ITEM_ARMOR_BOOTS, 4), LEGGINGS(MessageType.SHOP_ITEM_ARMOR_LEGGINGS,
			7), CHESTPLATE(MessageType.SHOP_ITEM_ARMOR_CHESTPLATE, 8), HELMET(MessageType.SHOP_ITEM_ARMOR_HELMET, 5);

	private final MessageType messageType;
	private final int materialAmount;

	private ArmorMerchandiseType(MessageType messageType, int materialAmount) {
		this.messageType = messageType;
		this.materialAmount = materialAmount;
	}

	public static ArmorMerchandiseType parse(String string) {
		for (ArmorMerchandiseType amt : ArmorMerchandiseType.values()) {
			if (!amt.getTranslated().equalsIgnoreCase(string))
				continue;
			return amt;
		}
		return null;
	}

	public String getTranslated() {
		return this.getMessageType().getTranslation().getValue(new Object[0]);
	}

	public MessageType getMessageType() {
		return this.messageType;
	}

	public int getMaterialAmount() {
		return this.materialAmount;
	}
}
